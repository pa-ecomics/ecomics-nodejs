/*
 * Project : ecomics-nodejs
 * FileName : Response.handler.js
 * Copyright (c) created by greg at 13.06.19
 * Description : handler to prepare response
 */

'use strict';

const Path = require('path');
const TokenService = require(Path.resolve('services', 'Token.service'));

module.exports = {
    /*
    * Create a response
    * @params: succeed boolean --> if the request has been finished with success
    * @params: oldToken string --> old token
    * @params: data string|Array -->  data to send in the response
    * @params: callback Function --> function all after create
    * Return: response Array --> response formatted
    */
    createResponse: (succeed, oldToken, data) => {
        let result = {
            succeed: succeed
        };

        if (succeed) {
            result.data = data;
        } else {
            result.message = typeof data === 'string' ? data : data.message;
        }

        if (oldToken !== null) {
            result.token = TokenService.sign({
                userId: oldToken ? TokenService.decode(oldToken).userId : data.id
            });
        }

        return result;
    }
};
