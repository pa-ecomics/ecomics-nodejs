/*
 * Project : ecomics-nodejs
 * FileName : index.js
 * Copyright (c) created by greg at 13.06.19
 * Description : index for pretty import
 */

const Path = require('path');

const handlersNames = ['Crud', 'Response'];
const handlers = {};

handlersNames.forEach(name => {
    handlers[name] = require(Path.resolve('handlers', name + '.handler'));
});

module.exports = handlers;
