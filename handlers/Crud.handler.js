/*
 * Project : ecomics-nodejs
 * FileName : Crud.handler.js
 * Copyright (c) created by greg at 13.06.19
 * Description : handler for crud
 */

const Path = require('path');
const errorMessage = require(Path.resolve('messages', 'error.json'));
const responseHandler = require(Path.resolve('handlers', 'Response.handler'));
const NumberService = require(Path.resolve('services', 'Number.service'));

module.exports = {
    /*
    * Get model by ID
    * @params: token string --> request token
    * @params: Model Model --> model to search
    * @params: tableId string|integer --> id to serach in model
    * Return: response created by response handler wih data
    */
    getByPk: (res, token, Model, tableId, options) => {
        if (NumberService.isInteger(tableId)) {
            Model.findByPk(tableId, options).then(result => {
                if (!result) {
                    return Promise.reject(errorMessage[Model.getTableName()].notFound);
                }

                res.json(responseHandler.createResponse(true, token, result));
            }).catch(error => {
                res.json(responseHandler.createResponse(false, token, error));
            });
        } else {
            res.json(responseHandler.createResponse(false, token, errorMessage.format.id));
        }
    },

    /*
    * Get all from model
    * @params: token string --> request token
    * @params: Model Model --> model to search
    * Return: response created by response handler with data
    */
    getAll: (res, token, Model, options) => {
        Model.findAll(options).then(result => {
            res.json(responseHandler.createResponse(true, token, result));
        }).catch(error => {
            res.json(responseHandler.createResponse(false, token, error));
        });
    },

    /*
    * Create a new instance of model
    * @params: token string --> request token
    * @params: Model Model --> model to create instance
    * @params: options Array --> option to create
    * @params: verifications Array --> check other model exist before create
    * @params: callback Function --> function all after create
    * Return: response created by response handler with data
    */
    create: (res, token, Model, options, verifications, callback) => {
        let promiseVerify = [];

        if (verifications) {
            verifications.forEach(verification => {
                promiseVerify.push(verification.Model.findByPk(verification.id));
            });
        }

        Promise.all(promiseVerify).then(resultVerify => {
            for (let index = 0; index < resultVerify.length; index++) {
                if (!resultVerify[index]) {
                    return Promise.reject(errorMessage[verifications[index].Model.getTableName()].notFound);
                }
            }

            return Model.create(options);
        }).then(async (newData) => {
            if (callback) {
                callback(newData);
            }
            res.json(responseHandler.createResponse(true, token, newData));
        }).catch(error => {
            res.json(responseHandler.createResponse(false, token, error));
        });
    },

    /*
    * Create a new instance of model
    * @params: token string --> request token
    * @params: Model Model --> model to create instance
    * @params: options Array --> option to create
    * @params: verifications Array --> check other model exist before create
    * @params: callback Function --> function all after create
    * Return: response created by response handler with data
    */
    findOrCreate: (res, token, Model, options, verifications, callback) => {
        let promiseVerify = [];

        if (verifications) {
            verifications.forEach(verification => {
                promiseVerify.push(verification.Model.findByPk(verification.id));
            });
        }

        Promise.all(promiseVerify).then(resultVerify => {
            for (let index = 0; index < resultVerify.length; index++) {
                if (!resultVerify[index]) {
                    return Promise.reject(errorMessage[verifications[index].Model.getTableName()].notFound);
                }
            }

            return Model.findOrCreate(options);
        }).then(async ([newData, created]) => {
            if (created) {
                if (callback) {
                    callback(newData);
                }

                res.json(responseHandler.createResponse(true, token, newData));
            } else {
                return Promise.reject(errorMessage[Model.getTableName()].exist);
            }
        }).catch(error => {
            res.json(responseHandler.createResponse(false, token, error));
        });
    },

    /*
    * Update an instance of model by id
    * @params: token string --> request token
    * @params: Model Model --> model to search instance to update
    * @params: tableId string|integer -->  search instance with this id in model
    * @params: updateData Array --> new data to update
    * @params: callback Function --> function all after create
    * Return: response created by response handler with data
    */
    updateByPk: (res, token, Model, tableId, updateData, callback) => {
        if (Object.keys(updateData).length || callback) {
            Model.findByPk(tableId).then(data => {
                if (!data) {
                    return Promise.reject(errorMessage[Model.getTableName()].notFound);
                } else {
                    return data.update(updateData);
                }
            }).then(updatedData => {
                if (callback) {
                    callback(updatedData);
                }

                res.json(responseHandler.createResponse(true, token, updatedData));
            }).catch(error => {
                res.json(responseHandler.createResponse(false, token, error));
            });
        } else {
            res.json(responseHandler.createResponse(false, token, errorMessage.nothingUpdated));
        }
    },

    /*
    * Delete an instance model by id
    * @params: token string --> request token
    * @params: Model Model --> model to search instance to delete
    * @params: tableId string|integer -->  search instance with this id in model
    * @params: callback Function --> function all after create
    * Return: response created by response handler with data
    */
    deleteByPk: (res, token, Model, tableId, callback) => {
        if (NumberService.isInteger(tableId)) {
            Model.destroy({
                where: {
                    id: tableId
                },
                limit: 1
            }).then(count => {
                if (callback) {
                    callback();
                }

                if (count) {
                    res.json(responseHandler.createResponse(true, token));
                } else {
                    return Promise.reject(errorMessage[Model.getTableName()].notFound);
                }
            }).catch(error => {
                res.json(responseHandler.createResponse(false, token, error));
            });
        } else {
            res.json(responseHandler.createResponse(false, token, errorMessage.format.id));
        }
    }
};
