/*
 * Project : ecomics-nodejs
 * FileName : picture.js
 * Copyright (c) created by greg at 13.06.19
 * Description : routes for picture
 */

const express = require('express');
const Path = require('path');
const router = express.Router();
const pictureController = require(Path.resolve('controllers', 'pictureController'));

/* picture crud */
router.route('/')
    .get(pictureController.get)
    .post(pictureController.create)
    .put(pictureController.update)
    .delete(pictureController.delete);

module.exports = router;
