/*
 * Project : ecomics-nodejs
 * FileName : user.js
 * Copyright (c) created by greg at 13.06.19
 * Description : routes for user
 */

const express = require('express');
const Path = require('path');
const router = express.Router();
const userController = require(Path.resolve('controllers', 'userController'));

/* user crud */
router.route('/')
    .get(userController.get)
    .post(userController.create)
    .put(userController.update)
    .delete(userController.delete);

/* get all user */
router.route('/all')
    .get(userController.getAll);

/* user role crud */
router.route('/role')
    .get(userController.getRole)
    .post(userController.addRole)
    .delete(userController.removeRole);

/* authentication */
router.route('/authenticate')
    .get(userController.authenticateAuto)
    .post(userController.authenticatePassword)
    .put(userController.authenticateSocialNetwork);

module.exports = router;
