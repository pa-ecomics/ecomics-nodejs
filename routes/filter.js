/*
 * Project : ecomics-nodejs
 * FileName : filter.js
 * Copyright (c) created by greg at 13.06.19
 * Description : routes for filter
 */

const express = require('express');
const Path = require('path');
const router = express.Router();
const filterController = require(Path.resolve('controllers', 'filterController'));

/* filter crud */
router.route('/')
    .post(filterController.create)
    .delete(filterController.delete);

/* filter get all */
router.route('/all')
    .get(filterController.getAll);

module.exports = router;
