/*
 * Project : ecomics-nodejs
 * FileName : page.js
 * Copyright (c) created by greg at 13.06.19
 * Description : routes for page
 */

const express = require('express');
const Path = require('path');
const router = express.Router();
const pageController = require(Path.resolve('controllers', 'pageController'));

/* page crud */
router.route('/')
    .get(pageController.get)
    .post(pageController.create)
    .delete(pageController.delete);

/* generate page with frale */
router.post('/generateWithFrame', pageController.generatePageWithFrame);

module.exports = router;
