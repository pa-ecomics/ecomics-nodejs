/*
 * Project : ecomics-nodejs
 * FileName : role.js
 * Copyright (c) created by greg at 13.06.19
 * Description : routes for role
 */

const express = require('express');
const Path = require('path');
const router = express.Router();
const roleController = require(Path.resolve('controllers', 'roleController'));

router.route('/')
    .get(roleController.get)
    .post(roleController.create)
    .put(roleController.update)
    .delete(roleController.delete);

router.route('/all')
    .get(roleController.getAll);

module.exports = router;
