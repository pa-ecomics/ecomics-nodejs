/*
 * Project : ecomics-nodejs
 * FileName : bd.js
 * Copyright (c) created by greg at 13.06.19
 * Description : routes for bd
 */

const express = require('express');
const Path = require('path');
const router = express.Router();
const bdController = require(Path.resolve('controllers', 'bdController'));

/* bd crud */
router.route('/')
    .get(bdController.get)
    .post(bdController.create)
    .delete(bdController.delete);

/* get mines bd */
router.route('/mines').get(bdController.mines);

/* download bd */
router.route('/download').get(bdController.download);

/* fill frame on page */
router.route('/fill-page').post(bdController.fillPage);
router.route('/fillOneFrame')
    .post(bdController.fillOneFrame)
    .delete(bdController.removeOneFrame);

module.exports = router;
