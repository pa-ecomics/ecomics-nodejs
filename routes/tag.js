/*
 * Project : ecomics-nodejs
 * FileName : tag.js
 * Copyright (c) created by greg at 13.06.19
 * Description : routes for tag
 */

const express = require('express');
const Path = require('path');
const router = express.Router();
const tagController = require(Path.resolve('controllers', 'tagController'));

/* tag crud */
router.route('/')
    .get(tagController.get)
    .post(tagController.create)
    .put(tagController.update)
    .delete(tagController.delete);

/* get all tags */
router.get('/all', tagController.getAll);

module.exports = router;
