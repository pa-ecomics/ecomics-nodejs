/*
 * Project : ecomics-nodejs
 * FileName : template.js
 * Copyright (c) created by greg at 13.06.19
 * Description : routes for template
 */

const express = require('express');
const Path = require('path');
const router = express.Router();
const templateController = require(Path.resolve('controllers', 'templateController'));

/* template crud */
router.route('/')
    .get(templateController.get)
    .post(templateController.create)
    .put(templateController.update)
    .delete(templateController.delete);

/* get all template */
router.get('/all', templateController.getAll);

/* template tag crud */
router.route('/tag')
    .get(templateController.getTag)
    .post(templateController.addTag)
    .delete(templateController.removeTag);

module.exports = router;
