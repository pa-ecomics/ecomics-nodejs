/*
 * Project : ecomics-nodejs
 * FileName : index.js
 * Copyright (c) created by greg at 13.06.19
 * Description : routes for index
 */

const express = require('express');
const router = express.Router();
const env = process.env.NODE_ENV || 'development';
const Path = require('path');
const TokenService = require(Path.resolve('services', 'Token.service'));
const FileService = require(Path.resolve('services', 'File.service'));
const PathService = require(Path.resolve('services', 'Path.service'));

const publicURL = PathService.getPublic(false, false);

/* GET home page. */
router.get('/', function (req, res) {
    res.render('documentation.twig', { publicURL: publicURL });
});

router.post('/test', function (req, res, next) {
    FileService.upload('miniature', req.files.miniature, req.body.id);

    res.json('ok');
});

router.get('/jwt-get', function (req, res) {
    let token = TokenService.sign({
        userId: 1
    });

    res.send(token);
});

router.get('/jwt-check', function (req, res) {
    let token = req.query.token;

    TokenService.verify(token).then(decoded => {
        res.send(decoded);
    }).catch(error => {
        res.send(error);
    });
});

router.get('/env', (req, res) => {
    res.send(env);
});

module.exports = router;
