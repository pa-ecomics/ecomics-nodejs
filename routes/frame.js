/*
 * Project : ecomics-nodejs
 * FileName : frame.js
 * Copyright (c) created by greg at 13.06.19
 * Description : routes for frame
 */

const express = require('express');
const Path = require('path');
const router = express.Router();
const frameController = require(Path.resolve('controllers', 'frameController'));

/* frame crud */
router.route('/')
    .get(frameController.get)
    .post(frameController.create)
    .put(frameController.update)
    .delete(frameController.delete);

/* delete frame image */
router.route('/image')
    .delete(frameController.deleteImage);

module.exports = router;
