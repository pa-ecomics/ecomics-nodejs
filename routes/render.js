/*
 * Project : ecomics-nodejs
 * FileName : render.js
 * Copyright (c) created by greg at 13.06.19
 * Description : routes for render
 */

const express = require('express');
const Path = require('path');
const router = express.Router();
const renderController = require(Path.resolve('controllers', 'renderController'));

router.route('/').get(renderController.page);

module.exports = router;
