FROM node:11.12.0

WORKDIR /Project

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV

COPY package.json /Project
COPY yarn.lock /Project
RUN yarn install

COPY . /Project

CMD [ "yarn", "start" ]
