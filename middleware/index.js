/*
 * Project : ecomics-nodejs
 * FileName : index.js
 * Copyright (c) created by greg at 13.06.19
 * Description : middleware
 */

'use strict';

const Path = require('path');
const responseHandler = require(Path.resolve('handlers', 'Response.handler'));
const TokenService = require(Path.resolve('services', 'Token.service'));
const errorMessage = require(Path.resolve('messages', 'error.json'));

module.exports = {
    /* set header to restriction to the api access */
    setHeader: (req, res, next) => {
        /* TODO: ADD RESTRICTION TO BE SECURE : add ip origin maybe ?? */
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Headers', 'Origin, Content-Type, Accept, Access-Control-Allow-Headers, Authorization, X-Requested-With');
        res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');

        next();
    },

    /* trim all params */
    trimParams: (req, res, next) => {
        let paramsName = req.method === 'GET' || req.method === 'DELETE' ? 'query' : 'body';

        for (let [key, value] of Object.entries(req[paramsName])) {
            req[paramsName][key] = value.toString().trim();
        }

        next();
    },

    /* Check token validity */
    checkToken: (req, res, next) => {
        let paramsName = req.method === 'GET' || req.method === 'DELETE' ? 'query' : 'body';

        let token;
        if (req[paramsName].token) {
            token = req[paramsName].token;
        } else if (req[paramsName].formData) {
            token = JSON.parse(req[paramsName].formData).token;
        }

        TokenService.verify(token).then((data) => {
            console.log('Token Verified !!');
            next();
        }).catch(error => {
            console.log('Token wrong !!');
            console.log(error);
            console.log(token);
            console.log('body');
            console.log(req.body);
            console.log('query');
            console.log(req.query);

            if (error === 'token expired') {
                error = errorMessage.jwt.expired;
            }
            res.send(responseHandler.createResponse(false, null, error));
        });
    }
};
