/*
 * Project : ecomics-nodejs
 * FileName : template.js
 * Copyright (c) created by greg at 13.06.19
 * Description : model for TEMPLATE
 */

'use strict';

const tableName = 'Template';

module.exports = (sequelize, DataTypes) => {
    const Template = sequelize.define(tableName, {
        title: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        },
        description: {
            type: DataTypes.STRING,
            allowNull: false
        }
    }, {
        defaultScope: {
            attributes: ['id', 'title', 'description', 'createdAt', 'updatedAt'],
            include: [
                {
                    association: 'User'
                },
                {
                    association: 'Filter'
                }, {
                    association: 'Tags',
                    through: {
                        attributes: []
                    }
                },
                {
                    association: 'Pages',
                    include: [
                        {
                            association: 'Frames'
                        }
                    ]
                }
            ]
        },
        scopes: {
            light: {
                attributes: ['id', 'title', 'description', 'createdAt', 'updatedAt']
            },
            withoutUser: {
                attributes: ['id', 'title', 'description', 'createdAt', 'updatedAt'],
                include: {
                    association: 'Tags',
                    through: {
                        attributes: []
                    }
                }
            },
            withPage: {
                attributes: ['id', 'title', 'description', 'createdAt', 'updatedAt'],
                include: {
                    association: 'Pages'
                }
            }
        },
        tableName: tableName,
        timestamps: true
    });

    Template.associate = function (models) {
        Template.belongsToMany(models.Tag, {
            through: models.Template_Tag
        });

        Template.hasMany(models.Page, {
            foreignKey: {
                name: 'templateId',
                allowNull: false
            }
        });

        Template.hasMany(models.Bd, {
            foreignKey: {
                name: 'templateId',
                allowNull: false
            }
        });

        Template.belongsTo(models.User.scope('light'), {
            foreignKey: 'userId'
        });

        Template.belongsTo(models.Filter, {
            foreignKey: 'filterId'
        });
    };

    Template.prototype.getCleanTags = function () {
        let cleanTags = [];

        return this.getTags().then(tags => {
            tags.forEach(tag => {
                cleanTags.push({ id: tag.id, label: tag.label });
            });

            return cleanTags.length ? cleanTags : null;
        });
    };

    return Template;
};
