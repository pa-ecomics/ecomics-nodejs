/*
 * Project : ecomics-nodejs
 * FileName : user.js
 * Copyright (c) created by greg at 13.06.19
 * Description : model for USER
 */

'use strict';

const Path = require('path');
const HashService = require(Path.resolve('services', 'Hash.service'));
const tableName = 'User';

module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define(tableName, {
        email: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true,
            validate: {
                isEmail: true
            }
        },
        password: {
            type: DataTypes.STRING
        },
        firstName: {
            type: DataTypes.STRING
        },
        lastName: {
            type: DataTypes.STRING
        },
        googleId: {
            type: DataTypes.STRING
        },
        facebookId: {
            type: DataTypes.STRING
        }
    }, {
        defaultScope: {
            attributes: ['id', 'email', 'firstName', 'lastName', 'googleId', 'facebookId'],
            include: [
                {
                    association: 'Templates',
                    scope: 'simple'
                },
                {
                    association: 'Roles',
                    through: {
                        attributes: []
                    }
                }
            ]
        },
        scopes: {
            light: {
                attributes: ['id', 'email', 'firstName', 'lastName']
            }
        },
        hooks: {
            beforeValidate: (user) => {
                if (user.password) {
                    user.password = HashService.crypt(user.password);
                }
            }
        },
        tableName: tableName
    });

    User.associate = function (models) {
        User.belongsToMany(models.Role, {
            through: models.User_Role
        });

        User.hasMany(models.Template.scope('withoutUser'), {
            foreignKey: {
                name: 'userId',
                allowNull: false
            }
        });

        User.hasMany(models.Bd, {
            foreignKey: {
                name: 'userId',
                allowNull: false
            }
        });
    };

    User.prototype.authenticate = function (testPassword) {
        return HashService.compare(testPassword, this.password);
    };

    User.prototype.getCleanRoles = function () {
        let cleanRole = [];

        return this.getRoles().then(roles => {
            roles.forEach(role => {
                cleanRole.push({ id: role.id, label: role.label });
            });

            return cleanRole.length ? cleanRole : null;
        });
    };

    return User;
};
