/*
 * Project : ecomics-nodejs
 * FileName : role.js
 * Copyright (c) created by greg at 13.06.19
 * Description : model for ROLE
 */

'use strict';

const tableName = 'Role';

module.exports = (sequelize, DataTypes) => {
    const Role = sequelize.define(tableName, {
        label: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        }
    }, {
        tableName: tableName
    }
    );

    Role.associate = function (models) {
        Role.belongsToMany(models.User, {
            through: models.User_Role
        });
    };

    return Role;
};
