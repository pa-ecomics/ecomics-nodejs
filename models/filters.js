/*
 * Project : ecomics-nodejs
 * FileName : filters.js
 * Copyright (c) created by greg at 13.06.19
 * Description : model for FILTER
 */

'use strict';

const tableName = 'Filter';

module.exports = (sequelize, DataTypes) => {
    const Filter = sequelize.define(tableName, {
        label: {
            type: DataTypes.STRING,
            allowNull: false
        },
        url: {
            type: DataTypes.STRING,
            allowNull: false
        }
    }, {
        tableName: tableName
    });

    Filter.associate = function (models) {
        Filter.hasMany(models.Template, {
            foreignKey: {
                name: 'filterId',
                allowNull: true
            }
        });
    };

    return Filter;
};
