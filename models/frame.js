/*
 * Project : ecomics-nodejs
 * FileName : frame.js
 * Copyright (c) created by greg at 13.06.19
 * Description : model for FRAME
 */

'use strict';

const tableName = 'Frame';

module.exports = (sequelize, DataTypes) => {
    const Frame = sequelize.define(tableName, {
        positionX: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        positionY: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        width: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        height: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        indication: {
            type: DataTypes.STRING
        },
        isTaken: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    }, {
        scopes: {
            template: {
                include: {
                    association: 'Page',
                    include: {
                        association: 'Template'
                    }
                }
            }
        },
        tableName: tableName
    });

    Frame.associate = function (models) {
        Frame.belongsTo(models.Page, {
            foreignKey: 'pageId'
        });
    };

    return Frame;
};
