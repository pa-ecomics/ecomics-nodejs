/*
 * Project : ecomics-nodejs
 * FileName : bd.js
 * Copyright (c) created by greg at 13.06.19
 * Description : model for BD
 */

'use strict';

const tableName = 'Bd';

module.exports = (sequelize, DataTypes) => {
    const Bd = sequelize.define(tableName, {}, {
        defaultScope: {
            include: [
                {
                    association: 'Template'
                }
            ]
        },
        tableName: tableName,
        timestamps: true
    });

    Bd.associate = function (models) {
        Bd.belongsTo(models.Template, {
            foreignKey: 'templateId'
        });
    };

    return Bd;
};
