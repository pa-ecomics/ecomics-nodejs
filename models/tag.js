/*
 * Project : ecomics-nodejs
 * FileName : tag.js
 * Copyright (c) created by greg at 13.06.19
 * Description : model for TAG
 */

'use strict';

const tableName = 'Tag';

module.exports = (sequelize, DataTypes) => {
    const Tag = sequelize.define(tableName, {
        label: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        }
    }, {
        defaultScope: {
            include: {
                association: 'Templates',
                through: {
                    attributes: []
                }
            }
        },
        tableName: tableName
    });

    Tag.associate = function (models) {
        Tag.belongsToMany(models.Template.scope('light'), {
            through: models.Template_Tag
        });
    };

    return Tag;
};
