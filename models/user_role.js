/*
 * Project : ecomics-nodejs
 * FileName : user_role.js
 * Copyright (c) created by greg at 13.06.19
 * Description : model for USER_ROLE
 */

'use strict';

const tableName = 'User_Role';

module.exports = (sequelize, DataTypes) => {
    const UserRole = sequelize.define(tableName, {}, {
        tableName: tableName
    });

    return UserRole;
};
