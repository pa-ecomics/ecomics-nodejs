/*
 * Project : ecomics-nodejs
 * FileName : template_tag.js
 * Copyright (c) created by greg at 13.06.19
 * Description : model for TEMPLATE_TAG
 */

'use strict';

const tableName = 'Template_Tag';

module.exports = (sequelize, DataTypes) => {
    const TemplateTag = sequelize.define(tableName, {}, {
        tableName: tableName
    });

    return TemplateTag;
};
