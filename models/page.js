/*
 * Project : ecomics-nodejs
 * FileName : page.js
 * Copyright (c) created by greg at 13.06.19
 * Description : model for PAGE
 */

'use strict';

const tableName = 'Page';

module.exports = (sequelize, DataTypes) => {
    const Page = sequelize.define(tableName, {
        number: {
            type: DataTypes.INTEGER,
            allowNull: false
        }
    }, {
        defaultScope: {
            include: [
                {
                    association: 'Frames'
                }
            ]
        },
        scopes: {
            test: {
                attributes: ['id']
            }
        },
        tableName: tableName
    });

    Page.associate = function (models) {
        Page.hasMany(models.Frame, {
            foreignKey: {
                name: 'pageId',
                allowNull: false
            }
        });

        Page.belongsTo(models.Template, {
            foreignKey: 'templateId'
        });
    };

    return Page;
};
