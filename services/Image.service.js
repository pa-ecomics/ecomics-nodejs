/*
 * Project : ecomics-nodejs
 * FileName : Image.service.js
 * Copyright (c) created by greg at 13.06.19
 * Description : service for image proccess
 */

const Path = require('path');
const PImage = require('pureimage');
const fs = require('fs');
const axios = require('axios');
const pageConfig = require(Path.resolve('config', 'global')).page;
const FileService = require(Path.resolve('services', 'File.service'));
const mergeImages = require(Path.resolve('package', 'merge-images'));
const Canvas = require('canvas');
const resizeBufferImage = require('resize-img');
const imagesToPdf = require('images-to-pdf');

module.exports = {
    /*
    * Create an empty page
    * @params: pagePath string --> path where create the page
    * Return: promise with response if page was created
    */
    createEmptyPage: (pagePath) => {
        FileService.createDir(pagePath);

        let page = PImage.make(pageConfig.width, pageConfig.height);
        let ctx = page.getContext('2d');

        ctx.fillStyle = pageConfig.backgroundColor;
        ctx.fillRect(0, 0, pageConfig.width, pageConfig.height);

        return PImage.encodePNGToStream(page, fs.createWriteStream(pagePath));
    },

    /*
   * Create a frame in a page
   * @params: pagePath string --> path where is the page to add the frame
   * @params: x string|int --> position in horizontal axe (x) where frame start
   * @params: y string|int --> position in vertical axe (y) where frame start
   * @params: width string|int --> width of frame
   * @params: height string|int --> height of frame
   * Return: promise with response if page was updated with frame
   */
    createFrame: (pagePath, x, y, width, height) => {
        return PImage.decodePNGFromStream(fs.createReadStream(pagePath)).then((img) => {
            let img2 = PImage.make(img.width, img.height);
            let ctx = img2.getContext('2d');

            /* COPY OLD PAGE */
            ctx.drawImage(img,
                0, 0, img.width, img.height, // source
                0, 0, img.width, img.height // destination
            );

            /* MODIFY PAGE */
            ctx.fillStyle = pageConfig.backgroundColor;
            ctx.fillRect(parseInt(x), parseInt(y), parseInt(width), parseInt(height));

            /* OVERRIDE OLD PAGE */
            return PImage.encodePNGToStream(img2, fs.createWriteStream(pagePath));
        }).catch(error => {
            return error;
        });
    },

    /*
   * Create border for many frames
   * @params: pagePath string --> path where is the page to add the border frame
   * @params: frames Frame[] --> array of frames to add border
   * Return: promise with response if page was updated with frame
   */
    createBorderFrames: async (pagePath, frames) => {
        return await PImage.decodePNGFromStream(fs.createReadStream(pagePath)).then((img) => {
            let img2 = PImage.make(img.width, img.height);
            let ctx = img2.getContext('2d');

            /* COPY OLD PAGE */
            ctx.drawImage(img,
                0, 0, img.width, img.height, // source
                0, 0, img.width, img.height // destination
            );

            /* MODIFY PAGE */
            ctx.fillStyle = pageConfig.frameFinalColor;
            frames.forEach(frame => {
                ctx.strokeRect(parseInt(frame.positionX), parseInt(frame.positionY), parseInt(frame.width), parseInt(frame.height));
            });

            /* OVERRIDE OLD PAGE */
            return PImage.encodePNGToStream(img2, fs.createWriteStream(pagePath));
        }).catch(error => {
            return error;
        });
    },

    /*
   * Create border for many frames
   * @params: pagePath string --> path where is the page to add the border frame
   * @params: frames Frame[] --> array of frames to add border
   * Return: promise with response if page was updated with frame
   */
    createManyFrames: (pagePath, frames) => {
        return PImage.decodePNGFromStream(fs.createReadStream(pagePath)).then((img) => {
            let img2 = PImage.make(img.width, img.height);
            let ctx = img2.getContext('2d');

            /* COPY OLD PAGE */
            ctx.drawImage(img,
                0, 0, img.width, img.height, // source
                0, 0, img.width, img.height // destination
            );

            /* MODIFY PAGE */
            ctx.fillStyle = pageConfig.frameTmpColor;

            frames.forEach(frame => {
                ctx.fillRect(parseInt(frame.positionX), parseInt(frame.positionY), parseInt(frame.width), parseInt(frame.height));
            });

            /* OVERRIDE OLD PAGE */
            return PImage.encodePNGToStream(img2, fs.createWriteStream(pagePath));
        }).catch(error => {
            return error;
        });
    },

    /*
    * merge images
    * @params: data Array --> mergeImage option (all image src with position x and y
    * @params: frames Frame[] --> array of frames to add border
    * Return: promise
   */
    merge: (data) => {
        return mergeImages(data, {
            Canvas: Canvas
        });
    },

    /*
    * Resize an image
    * @params: imageBuffer Buffer --> buffer of image to resize
    * @params: option Array --> array of new with to resize
    * Return: promise
    */
    resize (imageBuffer, option) {
        if (typeof imageBuffer === 'string') {
            imageBuffer = new Buffer.from(imageBuffer.replace(/^data:image\/\w+;base64,/, ''), 'base64');
        }

        return resizeBufferImage(imageBuffer, option);
    },

    /*
    * Make a pdf with all page of a bd
    * @params: allPages Array --> all page src of a bd
    * @params: bdFinalPath string --> path to write the pdf result
    * Return: promise
    */
    async bdToPdf (allPages, bdFinalPath) {
        await imagesToPdf(allPages, bdFinalPath);
    },

    /*
    * Update the image with the machine learning filter
    * @params: urlMachineApi string--> url to the machine learning filter
    * @params: base64 string --> old image in base64
    * Return: promise with the result of the machine learning in b64
    */
    machineLearningUpdate: (urlMachineApi, base64) => {
        if (typeof base64 !== 'string') {
            base64 = new Buffer.from(base64).toString('base64');
        }

        console.log('before request');
        let start = new Date();
        return axios.post(urlMachineApi, {
            image_data: base64
        }).then(response => {
            let end = new Date() - start;
            console.info('Request execution time: %dms', end);

            if (response.data && response.data.data.data_b64) {
                return response.data.data.data_b64;
            } else {
                return Promise.reject('treatment error');
            }
        }).catch(() => {
            let end = new Date() - start;
            console.info('Request execution time: %dms', end);

            return base64;
        });
    }
};
