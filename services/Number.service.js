/*
 * Project : ecomics-nodejs
 * FileName : Number.service.js
 * Copyright (c) created by greg at 13.06.19
 * Description : service for number proccess
 */

module.exports = {
    /*
    * Check if is a good integer
    * @params: number any --> number to check
    * Return: boolean --> true if it's a real number
    */
    isInteger: (number) => {
        return Number.isInteger(Number(number));
    }
};
