/*
 * Project : ecomics-nodejs
 * FileName : Token.service.js
 * Copyright (c) created by greg at 13.06.19
 * service for token process
 */

const jwt = require('jsonwebtoken');
const config = require('config').get('jwt');

module.exports = {
    /*
    * create a token with data
    * @params: data any --> data to add in the token
    * Return: string --> new token with data
    */
    sign: (data) => {
        return jwt.sign(data, config.secret, {
            algorithm: config.algorithm,
            expiresIn: config.expiresIn,
            noTimestamp: config.noTimestamp
        });
    },

    /*
    * check token
    * @params: token string --> token to check
    */
    verify: (token) => {
        return Promise.resolve(jwt.verify(
            token,
            config.secret, {
                algorithm: config.algorithm,
                expiresIn: config.expiresIn,
                noTimestamp: config.noTimestamp,
                ignoreExpiration: config.ignoreExpiration
            }, function (error, decoded) {
                if (error) {
                    return Promise.reject(error.message.replace('jwt', 'token'));
                }

                return Promise.resolve(decoded);
            }));
    },

    /*
    * decode token
    * @params: token string --> token to check
    * Return: any --> data stored in the token
    */
    decode: (token) => {
        return jwt.decode(token);
    }
};
