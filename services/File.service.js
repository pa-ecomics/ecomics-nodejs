/*
 * Project : ecomics-nodejs
 * FileName : File.service.js
 * Copyright (c) created by greg at 13.06.19
 * Description : Service for tree processing
 */

const Path = require('path');
const mkdirp = require('mkdirp');
const rimraf = require('rimraf');
const ncp = require('ncp');
const fs = require('fs');

module.exports = {
    /*
    * upload file at the path
    * @params: file string --> file to upload
    * @params: fullPath string --> path where file is upload
    */
    upload: (file, fullPath) => {
        let directory = Path.dirname(fullPath);

        mkdirp(directory, error => {
            if (!error) {
                file.mv(fullPath, function (error) {
                    if (error) {
                        return error;
                    } else {
                        return true;
                    }
                });
            }
        });
    },

    /*
    * Create directory
    * @params: directoryPath string --> directory path
    */
    createDir: (directoryPath) => {
        let path = Path.dirname(directoryPath);

        return mkdirp.sync(path);
    },

    /*
    * Delete directory recursive
    * @params: fullPath string --> path of the directory to delete
    */
    delete: (fullPath) => {
        rimraf(fullPath, () => {
            return true;
        });
    },

    /*
    * Copy directory recusrive
    * @params: source string --> path of directory to copy
    * @params: destination string --> path new directory
    */
    copyDir: (source, destination) => {
        ncp(source, destination);
    },

    /*
   * Write image from base64 to path
   * @params: fullPath string --> path of new image
   * @params: base64 string --> of new image
   */
    writeImgFromBase64: (fullPath, base64) => {
        let base64Data = base64.replace(/^data:image\/png;base64,/, '');

        fs.writeFile(fullPath, base64Data, 'base64', function (error) {
            return error;
        });
    }
};
