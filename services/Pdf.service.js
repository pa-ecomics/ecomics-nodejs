/*
 * Project : ecomics-nodejs
 * FileName : Pdf.service.js
 * Copyright (c) created by greg at 13.06.19
 * Description : service for pdf proccess
 */

const PDFDocument = require('pdfkit');
const fs = require('file-system');
const Path = require('path');

const PathService = require(Path.resolve('services', 'Path.service'));

const resourcePath = PathService.getPublic(true);

module.exports = {
    createPdf: (output, text) => {
        let fullPath = Path.join(resourcePath, 'pdf', output);

        const doc = new PDFDocument();
        doc.pipe(fs.createWriteStream(fullPath));

        doc.text(text, 100, 100);
        doc.end();
    }
};
