/*
 * Project : ecomics-nodejs
 * FileName : Hash.service.js
 * Copyright (c) created by greg at 13.06.19
 * Description : Service for encrypting
 */

const bcrypt = require('bcrypt');
const saltRound = 10;

module.exports = {
    /*
    * Hash a string
    * @params: toHash string --> string to hash
    * Return: string --> hash of string
    */
    crypt: (toHash) => {
        return bcrypt.hashSync(toHash, saltRound);
    },

    /*
    * Compare a string
    * @params: toHash string --> string to to hash
    * Return: boolean --> if equals true
    */
    compare: (toCompare, hash) => {
        return bcrypt.compareSync(toCompare, hash);
    }
};
