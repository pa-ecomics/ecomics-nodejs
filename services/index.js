/*
 * Project : ecomics-nodejs
 * FileName : index.js
 * Copyright (c) created by greg at 13.06.19
 * Description : for pretty import
 */

const Path = require('path');

const servicesNames = ['Hash', 'Mailer', 'Number', 'Pdf', 'Token', 'File', 'Path', 'Image'];
const services = {};

servicesNames.forEach(name => {
    services[name] = require(Path.resolve('services', name + '.service'));
});

module.exports = services;
