/*
 * Project : ecomics-nodejs
 * FileName : Mailer.service.js
 * Copyright (c) created by greg at 13.06.19
 * Description : service for mailer proccess
 */

const Path = require('path');
const nodemailer = require('nodemailer');
const errorMessage = require(Path.resolve('messages', 'error.json')).services.mailer;
const configMailer = require('config').get('mailer');

let transporter = nodemailer.createTransport({
    service: configMailer.service,
    auth: {
        user: configMailer.login,
        pass: configMailer.password
    }
});

module.exports = {
    /*
    * Send a mail
    * @params: mailOption Array--> params
    * Return: promise with the result of the send mailer
    */
    send: (mailOption) => {
        /* Check data before send */
        if (!mailOption.to) {
            return Promise.reject(new Error(errorMessage.missing.destination));
        } else if (!mailOption.subject) {
            return Promise.reject(new Error(errorMessage.missing.subject));
        } else if (!mailOption.text && !mailOption.html) {
            return Promise.reject(new Error(errorMessage.missing.body));
        }

        return transporter.sendMail(mailOption);
    }
};
