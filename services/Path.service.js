/*
 * Project : ecomics-nodejs
 * FileName : Path.service.js
 * Copyright (c) created by greg at 13.06.19
 * Description : service to return path
 */

const Path = require('path');
const PathArray = require('array-path-join');
const config = require(Path.resolve('config', 'global'));
const publicConfig = config.path.public;
const sharedConfig = config.path.shared;
const sharePath = PathArray(sharedConfig);

const Paths = {
    publicBack: PathArray(publicConfig),
    publicFront: publicConfig.slice(1).toString(),
    shared: sharePath,
    sharedTemplateBack: Path.join(sharePath, 'template'),
    sharedTemplateFront: Path.join(PathArray(sharedConfig.slice(1)), 'template'),
    sharedBdBack: Path.join(sharePath, 'bd'),
    sharedBdFront: Path.join(PathArray(sharedConfig.slice(1)), 'bd')

};

module.exports = {
    /*
    * Return: string --> root path
    */
    getRoot: (isBack) => {
        return '/';
    },

    /*
    * Return: string --> public path
    */
    getPublic: (isBack, isRoot) => {
        let path = isBack ? Paths.publicBack : Paths.publicFront;

        return isRoot ? rootPath(path) : path;
    },

    /*
    * Return: string --> template path
    */
    getTemplate: (isBack, isRoot) => {
        let path = isBack ? Paths.sharedTemplateBack : Paths.sharedTemplateFront;

        return isRoot ? rootPath(path) : path;
    },

    /*
    * Return: string --> bd path
    */
    getBd: (isBack, isRoot) => {
        let path = isBack ? Paths.sharedBdBack : Paths.sharedBdFront;

        return isRoot ? rootPath(path) : path;
    },

    /*
    * Return: string --> miniature path
    */
    getUploadMiniature: (templateId, isBack, isRoot) => {
        let templatePath = isBack ? Paths.sharedTemplateBack : Paths.sharedTemplateFront;

        let path = Path.join(templatePath, templateId.toString(), 'miniature' + config.upload.extension);

        return isRoot ? rootPath(path) : path;
    },

    /*
    * Return: string --> upload path
    */
    getUploadPage: (templateId, pageNumber, isBack, isRoot) => {
        let templatePath = isBack ? Paths.sharedTemplateBack : Paths.sharedTemplateFront;

        let path = Path.join(templatePath, templateId.toString(), 'page_' + pageNumber.toString() + config.upload.extension);

        return isRoot ? rootPath(path) : path;
    },

    /*
    * Return: string --> bd root directory
    */
    getBdBaseDir: (bdId, isBack, isRoot) => {
        let bdPath = isBack ? Paths.sharedBdBack : Paths.sharedBdFront;

        let path = Path.join(bdPath, bdId.toString());

        return isRoot ? rootPath(path) : path;
    },

    /*
    * Return: string --> bd upload path
    */
    getBdUploadPage: (bdId, pageNumber, isBack, isRoot) => {
        let templatePath = isBack ? Paths.sharedBdBack : Paths.sharedBdFront;

        let path = Path.join(templatePath, bdId.toString(), 'page_' + pageNumber.toString() + config.upload.extension);

        return isRoot ? rootPath(path) : path;
    }
};

function rootPath (path) {
    return '/' + path;
}
