# Ecomics NodeJS

##Version

    Node : v11.12.0
    Npm  : 6.7.0

## Deploiement

### Local 

    clone project
    run "npm install" (install dependancies)
    run "npm start" (start server)

### Container

    Build docker : "docker build -t image_node ."
    Run Docker : "docker run -p 3001:3001 --name NodeJS -d image_node"
    
### Environnement
    export NODE_ENV=production
    npm start
    
### Intégration continue
    
    DEV     : Run test
    Release : Run Test -> Deploiement
    Master  : Run Test -> Deploiement -> Création tag
    
## Routes

Toutes les routes sont accessible via la base "/shared/"

### User

Description : CRUD sur l'utilisateur

#### GET

Retourne l'utilisateur correspond à l'`identifiant` donnée

    id: integer required

#### POST

Créer l'utilisateur avec les informations donnée
    
    pseudo: string required
    password: string required
    
    Retour : L'utilisateur créé

#### PUT

#### DELETE


