/*
 * Project : ecomics-nodejs
 * FileName : 20190421095128-initial.js
 * Copyright (c) created by greg at 13.06.19
 * Description : init database with table and relation
 */

'use strict';

const Path = require('path');
const models = require(Path.resolve('models'));

module
    .exports = {
        up: (queryInterface, Sequelize) => {
            return Promise.all([
                queryInterface.createTable(models.User.tableName, models.User.rawAttributes),
                queryInterface.createTable(models.Role.tableName, models.Role.rawAttributes),
                queryInterface.createTable(models.Tag.tableName, models.Tag.rawAttributes),
                queryInterface.createTable(models.Filter.tableName, models.Filter.rawAttributes)
            ]).then(() => {
                return Promise.all([
                    queryInterface.createTable(models.User_Role.tableName, models.User_Role.rawAttributes),
                    queryInterface.createTable(models.Template.tableName, models.Template.rawAttributes)
                ]);
            }).then(() => {
                return Promise.all([
                    queryInterface.createTable(models.Template_Tag.tableName, models.Template_Tag.rawAttributes),
                    queryInterface.createTable(models.Page.tableName, models.Page.rawAttributes),
                    queryInterface.createTable(models.Bd.tableName, models.Bd.rawAttributes)
                ]);
            }).then(() => {
                return Promise.all([
                    queryInterface.createTable(models.Frame.tableName, models.Frame.rawAttributes)
                ]);
            }).then(() => {
                return Promise.all([
                    queryInterface.addConstraint(models.Page.tableName, ['number', 'templateId'], {
                        type: 'unique',
                        name: 'constraint_unique_page_number_template'
                    }),
                    queryInterface.addConstraint(models.Frame.tableName, ['positionX', 'positionY', 'pageId'], {
                        type: 'unique',
                        name: 'constraint_unique_frame_position_page'
                    }),
                    queryInterface.addConstraint(models.Filter.tableName, ['label', 'url'], {
                        type: 'unique',
                        name: 'constraint_unique_filter'
                    })
                ]);
            });
        },

        down: (queryInterface, Sequelize) => {
            return Promise.all([queryInterface.dropTable(models.Frame.tableName, models.Frame.rawAttributes)]).then(() => {
                return Promise.all([
                    queryInterface.dropTable(models.Template_Tag.tableName, models.Template_Tag.rawAttributes),
                    queryInterface.dropTable(models.Page.tableName, models.Page.rawAttributes),
                    queryInterface.dropTable(models.Bd.tableName, models.Bd.rawAttributes)
                ]);
            }).then(() => {
                return Promise.all([
                    queryInterface.dropTable(models.User_Role.tableName, models.User_Role.rawAttributes),
                    queryInterface.dropTable(models.Template.tableName, models.Template.rawAttributes)
                ]);
            }).then(() => {
                return Promise.all([
                    queryInterface.dropTable(models.User.tableName, models.User.rawAttributes),
                    queryInterface.dropTable(models.Role.tableName, models.Role.rawAttributes),
                    queryInterface.dropTable(models.Tag.tableName, models.Tag.rawAttributes),
                    queryInterface.dropTable(models.Filter.tableName, models.Filter.rawAttributes)
                ]);
            });
        }
    };
