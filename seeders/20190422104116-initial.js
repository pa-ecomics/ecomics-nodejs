/*
 * Project : ecomics-nodejs
 * FileName : 20190422104116-initial.js
 * Copyright (c) created by greg at 13.06.19
 * Description : seeder to init bdd data
 */

'use strict';

const data = {
    User: {
        tableName: 'User',
        rows: [
            {
                email: 'gpequery@thomeurope.com',
                password: '$2b$10$TxPpgXl0HLtxGMmT8wMYx.W8aJc3Jvz0MIZzbPQmLkn3VuyBp./Be',
                googleId: '123456789'
            }, {
                email: 'g.pequery@gmail.com',
                password: '$2b$10$TxPpgXl0HLtxGMmT8wMYx.W8aJc3Jvz0MIZzbPQmLkn3VuyBp./Be',
                firstName: 'Greg',
                lastName: 'PEQUERY'
            },
            {
                email: 'mmederosnunez@myges.fr',
                password: '$2b$10$TxPpgXl0HLtxGMmT8wMYx.W8aJc3Jvz0MIZzbPQmLkn3VuyBp./Be',
                firstName: 'Manuel',
                lastName: 'MEDEROS NUNEZ'
            }
        ]
    },
    Role: {
        tableName: 'Role',
        rows: [
            { label: 'Administrator' }
        ]
    },
    Tag: {
        tableName: 'Tag',
        rows: [
            { label: 'popular' },
            { label: 'short' },
            { label: 'long' },
            { label: 'funny' }
        ]
    },
    UserRole: {
        tableName: 'User_Role',
        rows: [
            { UserId: 2, RoleId: 1 },
            { UserId: 3, RoleId: 1 }
        ]
    },
    Filter: {
        tableName: 'Filter',
        rows: [
            {label: 'Noir et blanc', url: 'http://51.83.12.219:8041/machinelearning/bw_converter'},
            {label: 'Classic BD', url: 'http://51.83.12.219:8041/machinelearning/classic_bd_filter'}
        ]
    }
};

module.exports = {
    up: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.bulkInsert(data.User.tableName, data.User.rows, {}),
            queryInterface.bulkInsert(data.Role.tableName, data.Role.rows, {}),
            queryInterface.bulkInsert(data.Tag.tableName, data.Tag.rows, {}),
            queryInterface.bulkInsert(data.Filter.tableName, data.Filter.rows, {})
        ]).then(() => {
            return Promise.all([
                queryInterface.bulkInsert(data.UserRole.tableName, data.UserRole.rows, {})
            ]);
        });
    },

    down: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.bulkDelete(data.UserRole.tableName, data.UserRole.rows, {})
        ]).then(() => {
            return Promise.all([
                queryInterface.bulkDelete(data.User.tableName, data.User.rows, {}),
                queryInterface.bulkDelete(data.Role.tableName, data.Role.rows, {}),
                queryInterface.bulkDelete(data.Tag.tableName, data.Tag.rows, {}),
                queryInterface.bulkDelete(data.Filter.tableName, data.Filter.rows, {})
            ]);
        });
    }
};
