/*
 * Project : ecomics-nodejs
 * FileName : app.js
 * Copyright (c) created by greg at 13.06.19
 */

const createError = require('http-errors');
const express = require('express');
const Path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const bodyParser = require('body-parser');
const Middleware = require(Path.resolve('middleware'));
const PathService = require(Path.resolve('services', 'Path.service'));
const fileUpload = require('express-fileupload');

const app = express();
const rootUrl = PathService.getRoot();
const publicURL = PathService.getPublic(true);

// view engine setup
app.set('views', Path.resolve('views'));
app.set('view engine', 'twig');

app.use(favicon(Path.resolve(publicURL, 'favicon.ico')));
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

/* Check token for shared directory */
app.use((req, res, next) => {
    if (req.path.startsWith('shared')) {
        Middleware.checkToken(req, res, next);
    } else {
        next();
    }
});

app.use(express.static(Path.resolve('public')));

app.use(Middleware.setHeader);
app.use(Middleware.trimParams);
app.use(fileUpload());

/* Check token for route */
if (process.env.NODE_ENV !== 'development') {
    app.use(unless([
        {
            path: rootUrl,
            methods: ['GET']
        }, {
            path: Path.join(rootUrl, 'user', 'authenticate'),
            methods: ['POST', 'PUT']
        }, {
            path: Path.join(rootUrl, 'user'),
            methods: ['POST']
        }, {
            path: Path.join(rootUrl, 'jwt-get'),
            methods: ['GET']
        }, {
            path: Path.join(rootUrl, 'env'),
            methods: ['GET']
        }
    ], Middleware.checkToken));
}

/* Init router */
const routersData = [
    'index', 'user', 'role', 'template', 'tag', 'page', 'frame', 'bd', 'picture', 'render', 'filter'
];

routersData.forEach(routerName => {
    let router = require(Path.resolve('routes', routerName));
    let routerUseName = (routerName === 'index' ? rootUrl : Path.join(rootUrl, routerName));

    app.use(routerUseName, router);
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    let errorCode = err.status || 500;
    res.status(errorCode).send(errorCode + ' : ' + res.locals.message);
});

function unless (paths, middleware) {
    return (req, res, next) => {
        let needToken = true;

        paths.forEach(line => {
            if (line.path === req.path && line.methods.includes(req.method)) {
                needToken = false;
                return false;
            } else if (line.path === req.path) {
                return false;
            }
        });

        if (needToken) {
            return middleware(req, res, next);
        } else {
            return next();
        }
    };
}

module.exports = app;
