/*
 * Project : ecomics-nodejs
 * FileName : tagController.js
 * Copyright (c) created by greg at 13.06.19
 * Description : controller for tagRoutes
 */

const Path = require('path');
const HttpStatus = require('http-status-codes');
const models = require(Path.resolve('models'));
const services = require(Path.resolve('services'));
const handlers = require(Path.resolve('handlers'));
const errorMessage = require(Path.resolve('messages', 'error.json'));
const Tag = models.Tag;
const NumberService = services.Number;
const crudHandler = handlers.Crud;
const responseHandler = handlers.Response;

module.exports = {
    get: (req, res, next) => {
        let tagId = req.query.tagId;

        if (tagId) {
            crudHandler.getByPk(res, req.query.token, Tag, tagId);
        } else {
            next();
        }
    },

    getAll: (req, res) => {
        crudHandler.getAll(res, req.query.token, Tag);
    },

    create: (req, res, next) => {
        let label = req.body.label;

        if (label) {
            crudHandler.findOrCreate(res, req.body.token, Tag, {
                where: {
                    label: label
                }
            });
        } else {
            next();
        }
    },

    update: (req, res, next) => {
        let tagId = req.body.tagId;
        let newLabel = req.body.label;

        if (tagId) {
            if (NumberService.isInteger(tagId)) {
                let updateData = {};

                if (newLabel) {
                    updateData.label = newLabel;
                }

                crudHandler.updateByPk(res, req.body.token, Tag, tagId, updateData);
            } else {
                res.status(HttpStatus.BAD_REQUEST).json(responseHandler.createResponse(false, req.body.token, errorMessage.format.id));
            }
        } else {
            next();
        }
    },

    delete: (req, res, next) => {
        let tagId = req.query.tagId;

        if (tagId) {
            crudHandler.deleteByPk(res, req.query.token, Tag, tagId);
        } else {
            next();
        }
    }
};
