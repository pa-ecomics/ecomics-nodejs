/*
 * Project : ecomics-nodejs
 * FileName : filterController.js
 * Copyright (c) created by greg at 13.06.19
 * Description : controller for filterRoutes
 */

const Path = require('path');
const models = require(Path.resolve('models'));
const handlers = require(Path.resolve('handlers'));
const Filter = models.Filter;
const crudHandler = handlers.Crud;

module.exports = {
    getAll: (req, res) => {
        crudHandler.getAll(res, req.query.token, Filter);
    },

    create: (req, res, next) => {
        let filterLabel = req.body.filterLabel;
        let filterUrl = req.body.filterUrl;

        if (filterLabel && filterUrl) {
            crudHandler.findOrCreate(res, req.body.token, Filter, {
                where: {
                    label: filterLabel,
                    url: filterUrl
                }
            });
        } else {
            next();
        }
    },

    delete: (req, res, next) => {
        let filterId = req.query.filterId;

        if (filterId) {
            crudHandler.deleteByPk(res, req.query.token, Filter, filterId);
        } else {
            next();
        }
    }
};
