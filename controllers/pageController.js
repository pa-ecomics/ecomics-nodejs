/*
 * Project : ecomics-nodejs
 * FileName : pageController.js
 * Copyright (c) created by greg at 13.06.19
 * Description : controller for pageRoutes
 */

const Path = require('path');
const HttpStatus = require('http-status-codes');
const models = require(Path.resolve('models'));
const services = require(Path.resolve('services'));
const handlers = require(Path.resolve('handlers'));
const errorMessage = require(Path.resolve('messages', 'error.json'));
const Page = models.Page;
const Template = models.Template;
const Frame = models.Frame;
const NumberService = services.Number;
const FileService = services.File;
const PathService = services.Path;
const ImageService = services.Image;
const crudHandler = handlers.Crud;
const responseHandler = handlers.Response;

module.exports = {
    get: (req, res, next) => {
        let pageId = req.query.pageId;

        if (pageId) {
            crudHandler.getByPk(res, req.query.token, Page, pageId);
        } else {
            next();
        }
    },

    create: (req, res, next) => {
        let number = req.body.number;
        let templateId = req.body.templateId;

        if (number && templateId) {
            if (NumberService.isInteger(number) && NumberService.isInteger(templateId)) {
                let newPage;
                Template.findByPk(templateId).then(template => {
                    if (!template) {
                        return Promise.reject(errorMessage.Template.notFound);
                    }

                    return Page.findOrCreate({
                        where: {
                            number: number,
                            templateId: templateId
                        }
                    });
                }).then(([newResult, created]) => {
                    if (created) {
                        /* init page with global frame */
                        let pagePath = PathService.getUploadPage(templateId, number, true);
                        ImageService.createEmptyPage(pagePath);

                        newPage = newResult;
                        /* Create frame in bdd */
                        return Promise.all([
                            Frame.findOrCreate({
                                where: {
                                    positionX: 20,
                                    positionY: 20,
                                    pageId: newResult.id
                                },
                                defaults: {
                                    width: 512,
                                    height: 512
                                }
                            }),
                            Frame.findOrCreate({
                                where: {
                                    positionX: 552,
                                    positionY: 20,
                                    pageId: newResult.id
                                },
                                defaults: {
                                    width: 512,
                                    height: 512
                                }
                            }),
                            Frame.findOrCreate({
                                where: {
                                    positionX: 20,
                                    positionY: 552,
                                    pageId: newResult.id
                                },
                                defaults: {
                                    width: 512,
                                    height: 512
                                }
                            }),
                            Frame.findOrCreate({
                                where: {
                                    positionX: 552,
                                    positionY: 552,
                                    pageId: newResult.id
                                },
                                defaults: {
                                    width: 512,
                                    height: 512
                                }
                            }),
                            Frame.findOrCreate({
                                where: {
                                    positionX: 20,
                                    positionY: 1084,
                                    pageId: newResult.id
                                },
                                defaults: {
                                    width: 512,
                                    height: 512
                                }
                            }),
                            Frame.findOrCreate({
                                where: {
                                    positionX: 552,
                                    positionY: 1084,
                                    pageId: newResult.id
                                },
                                defaults: {
                                    width: 512,
                                    height: 512
                                }
                            })
                        ]);
                    } else {
                        return Promise.reject(errorMessage.Page.exist);
                    }
                }).then((framesPromiseResult) => {
                    let framesToCreate = [];
                    framesPromiseResult.forEach(([frame, created]) => {
                        if (created) {
                            framesToCreate.push(frame);
                        }
                    });

                    /* Create frame in file */
                    let pagePath = PathService.getUploadPage(templateId, newPage.number, true);
                    ImageService.createManyFrames(pagePath, framesToCreate);

                    return Page.findByPk(newPage.id);
                }).then(newPage => {
                    res.json(responseHandler.createResponse(true, req.body.token, newPage));
                }).catch(error => {
                    res.json(responseHandler.createResponse(false, req.body.token, error));
                });
            } else {
                res.status(HttpStatus.BAD_REQUEST).json(responseHandler.createResponse(false, req.body.token, errorMessage.format.id));
            }
        } else {
            next();
        }
    },

    update: (req, res, next) => {
        let pageId = req.body.pageId;

        if (pageId) {
            if (NumberService.isInteger(pageId)) {
                res.json(responseHandler.createResponse(true, req.body.token, 'ok'));
                // Page.findByPk(pageId).then(page => {
                //     if (!page) {
                //         return Promise.reject(errorMessage.Page.notFound);
                //     } else {
                //         res.json(responseHandler.createResponse(true, req.body.token, page));
                //     }
                // }).catch(error => {
                //     res.json(responseHandler.createResponse(false, req.body.token, error));
                // });
            } else {
                res.status(HttpStatus.BAD_REQUEST).json(responseHandler.createResponse(false, req.body.token, errorMessage.format.id));
            }
        } else {
            next();
        }
    },

    delete: (req, res, next) => {
        let pageId = req.query.pageId;

        if (pageId) {
            let token = req.query.token;

            if (NumberService.isInteger(pageId)) {
                Page.findByPk(pageId).then(page => {
                    if (!page) {
                        return Promise.reject(errorMessage.Page.notFound);
                    }

                    /* delete page in bdd */
                    return [page, page.destroy()];
                }).then(([oldPage, count]) => {
                    /* delete page in file */
                    let pagePath = PathService.getUploadPage(oldPage.templateId, oldPage.number, true);
                    FileService.delete(pagePath);

                    res.json(responseHandler.createResponse(true, token, true));
                }).catch(error => {
                    res.json(responseHandler.createResponse(false, token, error));
                });
            } else {
                res.json(responseHandler.createResponse(false, token, errorMessage.format.id));
            }
        } else {
            next();
        }
    },

    generatePageWithFrame: (req, res, next) => {
        let pageNumber = req.body.pageNumber;
        let templateId = req.body.templateId;
        let frames = req.files ? req.files.frames || null : null;

        if (templateId && pageNumber && frames) {
            Promise.all([
                Template.scope('light').findByPk(templateId),
                Page.findOne({
                    where: {
                        templateId: templateId,
                        number: pageNumber
                    }
                })
            ]).then(([template, page]) => {
                if (!template) {
                    return Promise.reject(errorMessage.Template.notFound);
                }

                if (!page) {
                    return Promise.reject(errorMessage.Page.notFound);
                }

                let pagePath = PathService.getUploadPage(templateId, pageNumber, true);
                let dataToMerge = [pagePath];

                /* add frame on page file */
                frames.forEach(frame => {
                    dataToMerge.push(frame.data);
                });

                return ImageService.merge(dataToMerge);
            }).then(base64 => {
                res.json(responseHandler.createResponse(true, req.body.token, base64));
            }).catch(error => {
                res.json(responseHandler.createResponse(false, req.body.token, error));
            });
        } else {
            next();
        }
    }
};
