/*
 * Project : ecomics-nodejs
 * FileName : templateController.js
 * Copyright (c) created by greg at 13.06.19
 * Description : controller for templateRoutes
 */

const Path = require('path');
const HttpStatus = require('http-status-codes');
const models = require(Path.resolve('models'));
const services = require(Path.resolve('services'));
const handlers = require(Path.resolve('handlers'));
const errorMessage = require(Path.resolve('messages', 'error.json'));
const Template = models.Template;
const User = models.User;
const Tag = models.Tag;
const Page = models.Page;
const Frame = models.Frame;
const NumberService = services.Number;
const ImageService = services.Image;
const FileService = services.File;
const PathService = services.Path;
const crudHandler = handlers.Crud;
const responseHandler = handlers.Response;

module.exports = {
    get: (req, res, next) => {
        let templateId = req.query.templateId;

        if (templateId) {
            crudHandler.getByPk(res, req.query.token, Template, templateId);
        } else {
            next();
        }
    },

    getAll: (req, res) => {
        crudHandler.getAll(res, req.query.token, Template);
    },

    create: (req, res, next) => {
        let data = req.body.formData ? JSON.parse(req.body.formData) : req.body;

        let title = data.title;
        let description = data.description;
        let userId = data.userId;
        let filterId = data.filterId !== 'none' ? data.filterId : null;
        let numberPage = parseInt(data.numberPage) >= 0 ? parseInt(data.numberPage) : 0;

        if (title && description && userId && req.files) {
            if (NumberService.isInteger(userId)) {
                let newTemplate;

                User.findByPk(userId).then(user => {
                    if (!user) {
                        return Promise.reject(errorMessage.User.notFound);
                    }

                    /* create template bdd */
                    return Template.findOrCreate({
                        where: {
                            title: title
                        },
                        defaults: {
                            userId: user.id,
                            filterId: filterId,
                            description: description
                        }
                    });
                }).then(([template, created]) => {
                    newTemplate = template;

                    let createPagePromise = [];
                    if (created) {
                        /* Create template file */
                        /* Create miniature if file  exist */
                        if (req.files && req.files.miniature) {
                            let fileObject = req.files.miniature.length ? req.files.miniature[0] : req.files.miniature;
                            let path = PathService.getUploadMiniature(template.id, true);

                            FileService.upload(fileObject, path);
                        }

                        /* Create X page with numberPage */
                        for (let index = 1; index <= numberPage; index++) {
                            createPagePromise.push(Page.findOrCreate({
                                where: {
                                    number: index,
                                    templateId: newTemplate.id
                                }
                            }));
                        }
                    } else {
                        return Promise.reject(errorMessage.Template.exist);
                    }

                    return Promise.all(createPagePromise);
                }).then(promises => {
                    let framesPromises = [];

                    /* For each page, create page file and frame */
                    promises.forEach(([page, created]) => {
                        if (created) {
                            let pagePath = PathService.getUploadPage(newTemplate.id, page.number, true);
                            ImageService.createEmptyPage(pagePath);

                            /* Prepare default frame */
                            framesPromises.push(Frame.scope('template').findOrCreate({
                                where: {
                                    positionX: 20,
                                    positionY: 20,
                                    pageId: page.id
                                },
                                defaults: {
                                    width: 512,
                                    height: 512
                                }
                            }), Frame.scope('template').findOrCreate({
                                where: {
                                    positionX: 552,
                                    positionY: 20,
                                    pageId: page.id
                                },
                                defaults: {
                                    width: 512,
                                    height: 512
                                }
                            }), Frame.scope('template').findOrCreate({
                                where: {
                                    positionX: 20,
                                    positionY: 552,
                                    pageId: page.id
                                },
                                defaults: {
                                    width: 512,
                                    height: 512
                                }
                            }), Frame.scope('template').findOrCreate({
                                where: {
                                    positionX: 552,
                                    positionY: 552,
                                    pageId: page.id
                                },
                                defaults: {
                                    width: 512,
                                    height: 512
                                }
                            }), Frame.scope('template').findOrCreate({
                                where: {
                                    positionX: 20,
                                    positionY: 1084,
                                    pageId: page.id
                                },
                                defaults: {
                                    width: 512,
                                    height: 512
                                }
                            }), Frame.scope('template').findOrCreate({
                                where: {
                                    positionX: 552,
                                    positionY: 1084,
                                    pageId: page.id
                                },
                                defaults: {
                                    width: 512,
                                    height: 512
                                }
                            }));
                        }
                    });

                    return Promise.all(framesPromises);
                }).then((framesPromise) => {
                    return Template.findByPk(newTemplate.id);
                }).then(template => {
                    res.json(responseHandler.createResponse(true, data.token, template));
                }).catch(error => {
                    res.json(responseHandler.createResponse(false, data.token, error));
                });
            } else {
                res.status(HttpStatus.BAD_REQUEST).json(responseHandler.createResponse(false, data.token, errorMessage.format.id));
            }
        } else {
            next();
        }
    },

    update: (req, res, next) => {
        let data = req.body.formData ? JSON.parse(req.body.formData) : req.body;

        let templateId = data.templateId;

        if (templateId) {
            if (NumberService.isInteger(templateId)) {
                let updateData = {};
                let newTitle = data.title;
                let newDescription = data.description;

                updateData.filterId = data.filterId !== 'none' ? data.filterId : null;
                if (newTitle) {
                    updateData.title = newTitle;
                }

                if (newDescription) {
                    updateData.description = newDescription;
                }

                /* update template bdd */
                crudHandler.updateByPk(res, data.token, Template, templateId, updateData, (updatedTemplate) => {
                    if (req.files && req.files.miniature) {
                        /* update template file */

                        let fileObject = req.files.miniature.length ? req.files.miniature[0] : req.files.miniature;
                        let path = PathService.getUploadMiniature(updatedTemplate.id, true);

                        FileService.upload(fileObject, path);
                    }
                });
            } else {
                res.status(HttpStatus.BAD_REQUEST).json(responseHandler.createResponse(false, data.token, errorMessage.format.id));
            }
        } else {
            next();
        }
    },

    delete:
        (req, res, next) => {
            let templateId = req.query.templateId;

            if (templateId) {
                if (NumberService.isInteger(templateId)) {
                    /* Delete template bdd */
                    Template.destroy({
                        where: {
                            id: templateId
                        },
                        limit: 1
                    }).then(count => {
                        if (count) {
                            res.json(responseHandler.createResponse(true, req.query.token));

                            /* Delete template file */
                            let templateUrl = PathService.getTemplate(true);
                            let templateDirectory = Path.join(templateUrl, templateId);

                            FileService.delete(templateDirectory);
                            return Page.destroy({ where: { templateId: null } });
                        } else {
                            return Promise.reject(errorMessage.Template.notFound);
                        }
                    }).then(count => {
                        return Frame.destroy({ where: { pageId: null } });
                    }).catch(error => {
                        res.json(responseHandler.createResponse(false, req.query.token, error));
                    });
                } else {
                    res.json(responseHandler.createResponse(false, req.query.token, errorMessage.format.id));
                }
            } else {
                next();
            }
        },

    getTag:
        (req, res, next) => {
            let templateId = req.query.templateId;

            if (templateId) {
                if (NumberService.isInteger(templateId)) {
                    Template.findByPk(templateId).then(template => {
                        if (!template) {
                            return Promise.reject(errorMessage.Template.notFound);
                        } else {
                            return template.getCleanTags();
                        }
                    }).then(tags => {
                        res.json(responseHandler.createResponse(true, req.query.token, tags));
                    }).catch(error => {
                        res.json(responseHandler.createResponse(false, req.query.token, error));
                    });
                } else {
                    res.status(HttpStatus.BAD_REQUEST).json(responseHandler.createResponse(false, req.query.token, errorMessage.format.id));
                }
            } else {
                next();
            }
        },

    addTag:
        (req, res, next) => {
            let templateId = req.body.templateId;
            let tagId = req.body.tagId;

            if (templateId && tagId) {
                if (NumberService.isInteger(templateId) && NumberService.isInteger(tagId)) {
                    Promise.all([
                        Template.findByPk(templateId),
                        Tag.findByPk(tagId)
                    ]).then(([template, tag]) => {
                        if (!template) {
                            return Promise.reject(errorMessage.Template.notFound);
                        } else if (!tag) {
                            return Promise.reject(errorMessage.Tag.notFound);
                        } else {
                            return template.addTags(tag);
                        }
                    }).then(added => {
                        if (!added) {
                            res.json(responseHandler.createResponse(false, req.body.token, errorMessage.Template.haveTag));
                        } else {
                            res.json(responseHandler.createResponse(true, req.body.token));
                        }
                    }).catch(error => {
                        res.json(responseHandler.createResponse(false, req.body.token, error));
                    });
                } else {
                    res.status(HttpStatus.BAD_REQUEST).json(responseHandler.createResponse(false, req.body.token, errorMessage.format.id));
                }
            } else {
                next();
            }
        },

    removeTag: (req, res, next) => {
        let templateId = req.query.templateId;
        let tagId = req.query.tagId;

        if (templateId && tagId) {
            if (NumberService.isInteger(templateId) && NumberService.isInteger(tagId)) {
                Promise.all([
                    Template.findByPk(templateId),
                    Tag.findByPk(tagId)
                ]).then(([template, tag]) => {
                    if (!template) {
                        return Promise.reject(errorMessage.Template.notFound);
                    } else if (!tag) {
                        return Promise.reject(errorMessage.Tag.notFound);
                    } else {
                        return template.removeTags(tag);
                    }
                }).then(() => {
                    res.json(responseHandler.createResponse(true, req.query.token));
                }).catch(error => {
                    res.json(responseHandler.createResponse(false, req.query.token, error));
                });
            } else {
                res.status(HttpStatus.BAD_REQUEST).json(responseHandler.createResponse(false, req.query.token, errorMessage.format.id));
            }
        } else {
            next();
        }
    }
}
;
