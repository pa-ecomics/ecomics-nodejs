/*
 * Project : ecomics-nodejs
 * FileName : bdController.js
 * Copyright (c) created by greg at 13.06.19
 * Description : controller for bdRoutes
 */

const Path = require('path');
const HttpStatus = require('http-status-codes');
const models = require(Path.resolve('models'));
const services = require(Path.resolve('services'));
const handlers = require(Path.resolve('handlers'));
const errorMessage = require(Path.resolve('messages', 'error.json'));
const Bd = models.Bd;
const User = models.User;
const Template = models.Template;
const Frame = models.Frame;
const NumberService = services.Number;
const FileService = services.File;
const PathService = services.Path;
const ImageService = services.Image;
const crudHandler = handlers.Crud;
const responseHandler = handlers.Response;

module.exports = {
    get: (req, res, next) => {
        let bdId = req.query.bdId;

        if (bdId) {
            crudHandler.getByPk(res, req.query.token, Bd, bdId);
        } else {
            next();
        }
    },

    create: (req, res, next) => {
        let userId = req.body.userId;
        let templateId = req.body.templateId;

        if (userId && templateId) {
            if (NumberService.isInteger(userId) && NumberService.isInteger(templateId)) {
                /* Create BD database */
                crudHandler.create(res, req.body.token, Bd, {
                    userId: userId,
                    templateId: templateId
                }, [
                    {
                        Model: User,
                        id: userId
                    },
                    {
                        Model: Template,
                        id: templateId
                    }
                ], (newBd) => {
                    /* Create BD file */
                    let templatePath = Path.join(PathService.getTemplate(true), templateId.toString());
                    let bdPath = Path.join(PathService.getBd(true), newBd.id.toString());

                    FileService.createDir(bdPath);
                    FileService.copyDir(templatePath, bdPath);
                });
            } else {
                res.status(HttpStatus.BAD_REQUEST).json(responseHandler.createResponse(false, req.body.token, errorMessage.format.id));
            }
        } else {
            next();
        }
    },

    delete: (req, res, next) => {
        let bdId = req.query.bdId;

        if (bdId) {
            console.log('BdId : ' + bdId);
            crudHandler.deleteByPk(res, req.query.token, Bd, bdId, () => {
                let bdDirPath = PathService.getBdBaseDir(bdId, true);
                FileService.delete(bdDirPath);
            });
        } else {
            console.log('next');
            next();
        }
    },

    fillPage: (req, res, next) => {
        let bdId = req.body.bdId;
        let pageNumber = req.body.pageNumber;
        let frames = req.files ? req.files.frames || null : null;

        if (bdId && pageNumber && frames) {
            if (NumberService.isInteger(bdId) && NumberService.isInteger(pageNumber)) {
                /* Prepare la page à merger les frames files */
                let pagePath = PathService.getBdUploadPage(bdId, pageNumber, true);
                let dataToMerge = [{ src: pagePath }];

                let promises = [Bd.findByPk(bdId)];

                /* Prepares allFrames request */
                for (let i = 0; i < frames.length; i++) {
                    let frameId = Path.parse(frames[i].name).name;

                    /* search all frames */
                    promises.push(Frame.scope('template').findByPk(frameId));
                }

                /* Request on allFrameId */
                Promise.all(promises).then(promiseData => {
                    let bd = promiseData.shift();

                    let promisesBuffer = [];
                    for (let i = 0; i < promiseData.length; i++) {
                        let currentFrame = promiseData[i];

                        /* If frame id is not for this bd */
                        if (currentFrame.Page.Template.id !== bd.templateId) {
                            return Promise.reject(errorMessage.Frame.notFound);
                        }

                        /* prepare resize eachBuffer promise & data for merge image on the page */
                        promisesBuffer.push(ImageService.resize(frames[i].data, { width: currentFrame.width, height: currentFrame.height }));
                        dataToMerge.push({
                            x: currentFrame.positionX,
                            y: currentFrame.positionY
                        });
                    }

                    return Promise.all(promisesBuffer);
                }).then(buffers => {
                    /* add new buffer on data To merge */
                    buffers.forEach((item, index) => {
                        dataToMerge[index + 1].src = item;
                    });

                    /* Merge all image */
                    return ImageService.merge(dataToMerge);
                }).then(base64 => {
                    FileService.writeImgFromBase64(pagePath, base64);

                    /* return b64 for d'ont wait writing file */
                    res.json(responseHandler.createResponse(true, req.body.token, base64));
                }).catch(error => {
                    res.json(responseHandler.createResponse(false, req.body.token, error));
                });
            } else {
                res.status(HttpStatus.BAD_REQUEST).json(responseHandler.createResponse(false, req.body.token, errorMessage.format.id));
            }
        } else {
            next();
        }
    },

    mines: (req, res, next) => {
        let userId = req.query.userId;

        if (userId) {
            if (NumberService.isInteger(userId)) {
                crudHandler.getAll(res, req.query.token, Bd, {
                    where: {
                        userId: userId
                    }
                });
            } else {
                res.status(HttpStatus.BAD_REQUEST).json(responseHandler.createResponse(false, req.query.token, errorMessage.format.id));
            }
        } else {
            next();
        }
    },

    download: (req, res, next) => {
        let bdId = req.query.bdId;
        if (bdId) {
            if (NumberService.isInteger(bdId)) {
                let allPage = [];

                Bd.findByPk(bdId).then(bd => {
                    let createBorder = [];
                    bd.Template.Pages.forEach(page => {
                        let pagePath = PathService.getBdUploadPage(bd.id, page.number, true);
                        let frames = page.Frames;

                        /* add border in each frame before download */
                        createBorder.push(ImageService.createBorderFrames(pagePath, frames));
                        allPage.push(pagePath);
                    });

                    return Promise.all(createBorder);
                }).then(bd => {
                    /* After add border, prepare pdf from all page */
                    ImageService.bdToPdf(allPage, Path.join(PathService.getBdBaseDir(bdId, true), 'download.pdf'));
                    res.json(responseHandler.createResponse(true, req.query.token, true));
                }).catch(error => {
                    res.json(responseHandler.createResponse(false, req.query.token, error));
                });
            } else {
                res.status(HttpStatus.BAD_REQUEST).json(responseHandler.createResponse(false, req.query.token, errorMessage.format.id));
            }
        } else {
            next();
        }
    },

    fillOneFrame (req, res, next) {
        let data = req.body.formData ? JSON.parse(req.body.formData) : req.body;
        let bdId = data.bdId;
        let frameId = data.frameId;

        if (bdId && frameId) {
            if (NumberService.isInteger(bdId) && NumberService.isInteger(frameId)) {
                let pagePath;
                let currentFrame;
                let dataToMerge = [];

                /* search frame */
                Frame.scope('template').findByPk(frameId).then(frame => {
                    if (!frame) {
                        return Promise.reject(errorMessage.Frame.notFound);
                    }

                    if (req.files && req.files.frameImage) {
                        let fileObject = req.files.frameImage.length ? req.files.frameImage[0] : req.files.frameImage;

                        /* Prepare pagePath and save frame for later */
                        pagePath = PathService.getBdUploadPage(bdId, frame.Page.number, true);
                        currentFrame = frame;

                        /* Resize image to the frames size */
                        return ImageService.resize(fileObject.data, { width: frame.width, height: frame.height });
                    } else if (data.file_base) {
                        return ImageService.resize(data.file_base, { width: frame.width, height: frame.height });
                    }
                }).then(bufferResize => {
                    /* Send image to the machine learning if filter exist else continue with same base64 */
                    if (currentFrame.Page.Template.Filter && currentFrame.Page.Template.Filter.url) {
                        return ImageService.machineLearningUpdate(currentFrame.Page.Template.Filter.url, bufferResize);
                    } else {
                        return bufferResize;
                    }
                }).then(base64Updated => {
                    /*  add all base64 to merge in the image at the frame position */
                    if (base64Updated) {
                        dataToMerge = [
                            { src: pagePath }, {
                                src: Buffer.from(base64Updated, 'base64'),
                                x: currentFrame.positionX,
                                y: currentFrame.positionY
                            }
                        ];

                        return ImageService.merge(dataToMerge);
                    } else {
                        return Promise.reject('Traitement error');
                    }
                }).then(base64 => {
                    /* override old page with new merge files */
                    if (base64) {
                        FileService.writeImgFromBase64(pagePath, base64);
                    }

                    res.json(responseHandler.createResponse(true, data.token, true));
                }).catch(error => {
                    res.json(responseHandler.createResponse(false, data.token, error));
                });
            } else {
                res.status(HttpStatus.BAD_REQUEST).json(responseHandler.createResponse(false, data.token, errorMessage.format.id));
            }
        } else {
            next();
        }
    },

    removeOneFrame (req, res, next) {
        let bdId = req.query.bdId;
        let frameId = req.query.frameId;

        if (bdId && frameId) {
            if (NumberService.isInteger(bdId) && NumberService.isInteger(frameId)) {
                Frame.scope('template').findByPk(frameId).then(frame => {
                    let pagePath = PathService.getBdUploadPage(bdId, frame.Page.number, true);

                    /* to delete, replace with a white rectangle */
                    return ImageService.createFrame(pagePath, frame.positionX, frame.positionY, frame.width, frame.height);
                }).then(imageResponse => {
                    res.json(responseHandler.createResponse(true, req.query.token, true));
                }).catch(error => {
                    res.json(responseHandler.createResponse(false, req.query.token, error));
                });
            } else {
                res.status(HttpStatus.BAD_REQUEST).json(responseHandler.createResponse(false, req.query.token, errorMessage.format.id));
            }
        } else {
            next();
        }
    }
};
