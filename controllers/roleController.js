/*
 * Project : ecomics-nodejs
 * FileName : roleController.js
 * Copyright (c) created by greg at 13.06.19
 * Description : controller for roleRoutes
 */

const Path = require('path');
const HttpStatus = require('http-status-codes');
const models = require(Path.resolve('models'));
const services = require(Path.resolve('services'));
const handlers = require(Path.resolve('handlers'));
const errorMessage = require(Path.resolve('messages', 'error.json'));
const Role = models.Role;
const NumberService = services.Number;
const crudHandler = handlers.Crud;
const responseHandler = handlers.Response;

module.exports = {
    get: (req, res, next) => {
        let roleId = req.query.roleId;

        if (roleId) {
            crudHandler.getByPk(res, req.query.token, Role, roleId);
        } else {
            next();
        }
    },

    getAll: (req, res) => {
        crudHandler.getAll(res, req.query.token, Role);
    },

    create: (req, res, next) => {
        let label = req.body.label;

        if (label) {
            crudHandler.findOrCreate(res, req.body.token, Role, {
                where: {
                    label: label
                }
            });
        } else {
            next();
        }
    },

    update: (req, res, next) => {
        let roleId = req.body.roleId;
        let newLabel = req.body.label;

        if (roleId) {
            if (NumberService.isInteger(roleId)) {
                let updateData = {};

                if (newLabel) {
                    updateData.label = newLabel;
                }

                crudHandler.updateByPk(res, req.body.token, Role, roleId, updateData);
            } else {
                res.status(HttpStatus.BAD_REQUEST).json(responseHandler.createResponse(false, req.body.token, errorMessage.format.id));
            }
        } else {
            next();
        }
    },

    delete: (req, res, next) => {
        let roleId = req.query.roleId;

        if (roleId) {
            crudHandler.deleteByPk(res, req.query.token, Role, roleId);
        } else {
            next();
        }
    }
};
