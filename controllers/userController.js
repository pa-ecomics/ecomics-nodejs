/*
 * Project : ecomics-nodejs
 * FileName : userController.js
 * Copyright (c) created by greg at 13.06.19
 * Description : controller for userRoutes
 */

const Path = require('path');
const Sequelize = require('sequelize');
const HttpStatus = require('http-status-codes');
const models = require(Path.resolve('models'));
const services = require(Path.resolve('services'));
const handlers = require(Path.resolve('handlers'));
const errorMessage = require(Path.resolve('messages', 'error.json'));
const User = models.User;
const Role = models.Role;
const NumberService = services.Number;
const TokenService = services.Token;
const crudHandler = handlers.Crud;
const responseHandler = handlers.Response;

module.exports = {
    get: (req, res, next) => {
        let userId = req.query.userId;

        if (userId) {
            crudHandler.getByPk(res, req.query.token, User, userId);
        } else {
            next();
        }
    },

    getAll: (req, res) => {
        crudHandler.getAll(res, req.query.token, User);
    },

    create: (req, res, next) => {
        let email = req.body.email;
        let password = req.body.password;

        if (email && password) {
            crudHandler.findOrCreate(res, req.body.token, User, {
                where: {
                    email: email
                },
                defaults: {
                    password: password,
                    firstName: req.body.firstName,
                    lastName: req.body.lastName
                }
            });
        } else {
            next();
        }
    },

    update: (req, res, next) => {
        let userId = req.body.userId;

        if (userId) {
            if (NumberService.isInteger(userId)) {
                User.findOne({
                    where: {
                        id: { [Sequelize.Op.ne]: userId },
                        email: req.body.email || null
                    }
                }).then(user => {
                    if (user) {
                        return Promise.reject(errorMessage.User.exist);
                    }

                    return User.findByPk(userId);
                }).then(user => {
                    if (!user) {
                        return Promise.reject(errorMessage.User.notFound);
                    } else {
                        let newEmail = req.body.email;
                        let newPassword = req.body.password;
                        let newFirstName = req.body.firstName;
                        let newLastName = req.body.lastName;

                        let updateData = {};

                        if (newEmail) {
                            updateData.email = newEmail;
                        }

                        if (newPassword) {
                            updateData.password = newPassword;
                        }

                        if (newFirstName) {
                            updateData.firstName = newFirstName;
                        }

                        if (newLastName) {
                            updateData.lastName = newLastName;
                        }

                        return user.update(updateData);
                    }
                }).then(updatedData => {
                    res.json(responseHandler.createResponse(true, req.query.token, updatedData));
                }).catch(error => {
                    res.json(responseHandler.createResponse(false, req.query.token, error));
                });
            } else {
                res.status(HttpStatus.BAD_REQUEST).json(responseHandler.createResponse(false, req.body.token, errorMessage.format.id));
            }
        } else {
            next();
        }
    },

    delete: (req, res, next) => {
        let userId = req.query.userId;

        if (userId) {
            crudHandler.deleteByPk(res, req.query.token, User, userId);
        } else {
            next();
        }
    },

    getRole: (req, res, next) => {
        let userId = req.query.userId;

        if (userId) {
            if (NumberService.isInteger(userId)) {
                User.findByPk(userId).then(user => {
                    if (!user) {
                        return Promise.reject(errorMessage.User.notFound);
                    } else {
                        return user.getCleanRoles();
                    }
                }).then(roles => {
                    res.json(responseHandler.createResponse(true, req.query.token, roles));
                }).catch(error => {
                    res.json(responseHandler.createResponse(false, req.query.token, error));
                });
            } else {
                res.status(HttpStatus.BAD_REQUEST).json(responseHandler.createResponse(false, req.body.token, errorMessage.format.id));
            }
        } else {
            next();
        }
    },

    addRole: (req, res, next) => {
        let userId = req.body.userId;
        let roleId = req.body.roleId;

        if (userId && roleId) {
            if (NumberService.isInteger(userId) && NumberService.isInteger(roleId)) {
                Promise.all([
                    User.findByPk(userId),
                    Role.findByPk(roleId)
                ]).then(([user, role]) => {
                    if (!user) {
                        return Promise.reject(errorMessage.User.notFound);
                    } else if (!role) {
                        return Promise.reject(errorMessage.Role.notFound);
                    } else {
                        return user.addRoles(role);
                    }
                }).then((added) => {
                    if (added) {
                        res.json(responseHandler.createResponse(true, req.body.token));
                    } else {
                        res.json(responseHandler.createResponse(false, req.body.token, errorMessage.User.haveRole));
                    }
                }).catch(error => {
                    res.json(responseHandler.createResponse(false, req.body.token, error));
                });
            } else {
                res.status(HttpStatus.BAD_REQUEST).json(responseHandler.createResponse(false, req.body.token, errorMessage.format.id));
            }
        } else {
            next();
        }
    },

    removeRole: (req, res, next) => {
        let userId = req.query.userId;
        let roleId = req.query.roleId;

        if (userId && roleId) {
            if (NumberService.isInteger(userId) && NumberService.isInteger(roleId)) {
                Promise.all([
                    User.findByPk(userId),
                    Role.findByPk(roleId)
                ]).then(([user, role]) => {
                    if (!user) {
                        return Promise.reject(errorMessage.User.notFound);
                    } else if (!role) {
                        return Promise.reject(errorMessage.Role.notFound);
                    } else {
                        return user.removeRoles(role);
                    }
                }).then(removed => {
                    res.json(responseHandler.createResponse(true, req.query.token));
                }).catch(error => {
                    res.json(responseHandler.createResponse(false, req.query.token, error));
                });
            } else {
                res.status(HttpStatus.BAD_REQUEST).json(responseHandler.createResponse(false, req.query.token, errorMessage.format.id));
            }
        } else {
            next();
        }
    },

    authenticatePassword: (req, res, next) => {
        let email = req.body.email;
        let password = req.body.password;

        if (email && password) {
            User.findOne({
                where: {
                    email: email,
                    password: { [Sequelize.Op.ne]: null }
                },
                attributes: ['id', 'email', 'password', 'firstName', 'lastName']
            }).then(user => {
                if (!user) {
                    return Promise.reject(errorMessage.User.notFound);
                } else if (!user.authenticate(password)) {
                    return Promise.reject(errorMessage.User.badPassword);
                } else {
                    let userClean = Object.assign({}, user.get());
                    delete userClean.password;

                    res.json(responseHandler.createResponse(true, undefined, userClean));
                }
            }).catch(error => {
                res.json(responseHandler.createResponse(false, null, error));
            });
        } else {
            next();
        }
    },

    authenticateSocialNetwork: (req, res, next) => {
        let email = req.body.email;
        let socialName = req.body.socialName;
        let socialId = req.body.socialId;

        if (email && socialName && socialId) {
            socialName += 'Id';

            let options = {
                where: {
                    email: email
                },
                defaults: {
                    firstName: req.body.firstName,
                    lastName: req.body.lastName
                }
            };

            options.attributes = ['id', 'email', 'firstName', 'lastName', socialName];
            options.defaults[socialName] = socialId;

            User.findOrCreate(options).spread((user, created) => {
                if (created) {
                    return user;
                } else if (!user[socialName] || user[socialName] === socialId) {
                    let options = {};

                    if (!user[socialName]) {
                        options[socialName] = socialId;
                    }

                    if (!user.firstName) {
                        options.firstName = req.body.firstName;
                    }

                    if (!user.firstName) {
                        options.lastName = req.body.lastName;
                    }

                    return user.update(options);
                } else {
                    return Promise.reject(errorMessage.User.badPassword);
                }
            }).then(updatedUser => {
                res.json(responseHandler.createResponse(true, undefined, updatedUser));
            }).catch(error => {
                res.json(responseHandler.createResponse(false, null, error));
            });
        } else {
            next();
        }
    },

    authenticateAuto: (req, res, next) => {
        let token = req.query.token;

        if (token) {
            let decoded = TokenService.decode(token);

            if (decoded) {
                crudHandler.getByPk(res, req.query.token, User, decoded.userId);
            } else {
                res.json(responseHandler.createResponse(false, null, errorMessage.jwt.bad));
            }
        } else {
            next();
        }
    }
};
