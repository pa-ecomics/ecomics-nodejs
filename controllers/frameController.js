/*
 * Project : ecomics-nodejs
 * FileName : frameController.js
 * Copyright (c) created by greg at 13.06.19
 * Description : controller for frameRoutes
 */

const Path = require('path');
const HttpStatus = require('http-status-codes');
const models = require(Path.resolve('models'));
const services = require(Path.resolve('services'));
const handlers = require(Path.resolve('handlers'));
const errorMessage = require(Path.resolve('messages', 'error.json'));
const Frame = models.Frame;
const Page = models.Page;
const NumberService = services.Number;
const PathService = services.Path;
const ImageService = services.Image;
const FileService = services.File;
const crudHandler = handlers.Crud;
const responseHandler = handlers.Response;

module.exports = {
    get: (req, res, next) => {
        let frameId = req.query.frameId;

        if (frameId) {
            crudHandler.getByPk(res, req.query.token, Frame, frameId);
        } else {
            next();
        }
    },

    create: (req, res, next) => {
        let positionX = req.body.positionX;
        let positionY = req.body.positionY;
        let width = req.body.width;
        let height = req.body.height;
        let pageId = req.body.pageId;

        if (positionX && positionY && width && height && pageId) {
            if (NumberService.isInteger(pageId)) {
                Page.findByPk(pageId).then((page) => {
                    if (!page) {
                        return Promise.reject(errorMessage.Page.notFound);
                    }

                    return page.getTemplate();
                }).then(template => {
                    if (!template) {
                        return Promise.reject(errorMessage.Template.notFound);
                    }

                    return Frame.findOrCreate({
                        where: {
                            positionX: positionX,
                            positionY: positionY,
                            pageId: pageId
                        },
                        defaults: {
                            width: width,
                            height: height
                        }
                    });
                }).then(([frame, created]) => {
                    res.json(responseHandler.createResponse(true, req.body.token, frame));
                }).catch(error => {
                    res.json(responseHandler.createResponse(false, req.body.token, error));
                });
            } else {
                res.status(HttpStatus.BAD_REQUEST).json(responseHandler.createResponse(false, req.body.token, errorMessage.format.id));
            }
        } else {
            next();
        }
    },

    update: (req, res, next) => {
        let data = req.body.formData ? JSON.parse(req.body.formData) : req.body;

        let frameId = data.frameId;
        let newPositionX = data.positionX;
        let newPositionY = data.positionY;
        let newWidth = data.width;
        let newHeight = data.height;
        let newIndication = data.indication;

        if (frameId) {
            if (NumberService.isInteger(frameId)) {
                let updateData = {};

                if (newPositionX) {
                    updateData.positionX = newPositionX;
                }

                if (newPositionY) {
                    updateData.positionY = newPositionY;
                }

                if (newWidth) {
                    updateData.width = newWidth;
                }

                if (newHeight) {
                    updateData.height = newHeight;
                }

                if (newIndication !== undefined) {
                    updateData.indication = newIndication || null;
                }

                if (req.files && req.files.frameImage) {
                    updateData.isTaken = true;
                }

                if (Object.keys(updateData).length || (req.files && req.files.frameImage)) {
                    let dataToMerge = [];
                    let pagePath;
                    let currentFrame;

                    Frame.scope('template').findByPk(frameId).then(frame => {
                        if (!frame) {
                            return Promise.reject(errorMessage.Frame.notFound);
                        } else {
                            return frame.update(updateData);
                        }
                    }).then(updatedFrame => {
                        /* resize fill to frame size */
                        if (req.files && req.files.frameImage) {
                            let fileObject = req.files.frameImage.length ? req.files.frameImage[0] : req.files.frameImage;
                            pagePath = PathService.getUploadPage(updatedFrame.Page.Template.id, updatedFrame.Page.number, true);
                            currentFrame = updatedFrame;

                            return ImageService.resize(fileObject.data, { width: updatedFrame.width, height: updatedFrame.height });
                        } else if (data.file_base) {
                            return ImageService.resize(data.file_base, { width: updatedFrame.width, height: updatedFrame.height });
                        }
                    }).then(bufferResize => {
                        /* merge resize file to page file at frame position */
                        if (bufferResize) {
                            dataToMerge = [
                                { src: pagePath }, {
                                    src: bufferResize,
                                    x: currentFrame.positionX,
                                    y: currentFrame.positionY
                                }
                            ];

                            return ImageService.merge(dataToMerge);
                        }
                    }).then(base64 => {
                        /* override old page with new merge files */
                        if (base64) {
                            FileService.writeImgFromBase64(pagePath, base64);
                        }

                        res.json(responseHandler.createResponse(true, data.token, currentFrame));
                    }).catch(error => {
                        res.json(responseHandler.createResponse(false, data.token, error));
                    });
                } else {
                    res.json(responseHandler.createResponse(false, data.token, errorMessage.nothingUpdated));
                }
            } else {
                res.status(HttpStatus.BAD_REQUEST).json(responseHandler.createResponse(false, data.token, errorMessage.format.id));
            }
        } else {
            next();
        }
    },

    delete: (req, res, next) => {
        let frameId = req.query.frameId;

        if (frameId) {
            crudHandler.deleteByPk(res, req.query.token, Frame, frameId);
        } else {
            next();
        }
    },

    deleteImage: (req, res, next) => {
        let frameId = req.query.frameId;

        if (frameId) {
            if (NumberService.isInteger(frameId)) {
                Frame.scope('template').findByPk(frameId).then(frame => {
                    let pagePath = PathService.getUploadPage(frame.Page.Template.id, frame.Page.number, true);

                    /* to delete, create a white rectangle one the page at the frame position */
                    return Promise.all([
                        ImageService.createFrame(pagePath, frame.positionX, frame.positionY, frame.width, frame.height),
                        frame.update({ isTaken: false })
                    ]);
                }).then(([imageResponse, frameUpdated]) => {
                    res.json(responseHandler.createResponse(true, req.query.token, frameUpdated));
                }).catch(error => {
                    res.json(responseHandler.createResponse(false, req.query.token, error));
                });
            } else {
                res.status(HttpStatus.BAD_REQUEST).json(responseHandler.createResponse(false, req.query.token, errorMessage.format.id));
            }
        } else {
            next();
        }
    }
};
