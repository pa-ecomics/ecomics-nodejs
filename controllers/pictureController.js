/*
 * Project : ecomics-nodejs
 * FileName : pictureController.js
 * Copyright (c) created by greg at 13.06.19
 * Description : controller for pictureRoutes
 */

const Path = require('path');
const HttpStatus = require('http-status-codes');
const models = require(Path.resolve('models'));
const services = require(Path.resolve('services'));
const handlers = require(Path.resolve('handlers'));
const errorMessage = require(Path.resolve('messages', 'error.json'));
const Picture = models.Picture;
const Bd = models.Bd;
const Frame = models.Frame;
const NumberService = services.Number;
const crudHandler = handlers.Crud;
const responseHandler = handlers.Response;

module.exports = {
    get: (req, res, next) => {
        let pictureId = req.query.pictureId;

        if (pictureId) {
            crudHandler.getByPk(res, req.query.token, Picture, pictureId);
        } else {
            next();
        }
    },

    create: (req, res, next) => {
        let file = req.body.file;
        let bdId = req.body.bdId;
        let frameId = req.body.frameId;

        if (file && bdId && frameId) {
            if (NumberService.isInteger(bdId) && NumberService.isInteger(frameId)) {
                crudHandler.findOrCreate(res, req.body.token, Picture, {
                    where: {
                        bdId: bdId,
                        frameId: frameId
                    },
                    defaults: {
                        file: file
                    }
                }, [
                    {
                        Model: Bd,
                        id: bdId
                    },
                    {
                        Model: Frame,
                        id: frameId
                    }
                ]);
            } else {
                res.status(HttpStatus.BAD_REQUEST).json(responseHandler.createResponse(false, req.body.token, errorMessage.format.id));
            }
        } else {
            next();
        }
    },

    update: (req, res, next) => {
        let pictureId = req.body.pictureId;
        let newFile = req.body.file;

        if (pictureId) {
            if (NumberService.isInteger(pictureId)) {
                let updateData = {};

                if (newFile) {
                    updateData.file = newFile;
                }

                crudHandler.updateByPk(res, req.body.token, Picture, pictureId, updateData);
            } else {
                res.status(HttpStatus.BAD_REQUEST).json(responseHandler.createResponse(false, req.body.token, errorMessage.format.id));
            }
        } else {
            next();
        }
    },

    delete: (req, res, next) => {
        let pictureId = req.query.pictureId;

        if (pictureId) {
            crudHandler.deleteByPk(res, req.query.token, Picture, pictureId);
        } else {
            next();
        }
    }
};
