/*
 * Project : ecomics-nodejs
 * FileName : renderController.js
 * Copyright (c) created by greg at 13.06.19
 * Description : controller for renderRoutes
 */

const Path = require('path');
const models = require(Path.resolve('models'));
const handlers = require(Path.resolve('handlers'));
const Page = models.Page;
const responseHandler = handlers.Response;

module.exports = {
    page: (req, res, next) => {
        let token = req.query.token;
        let pageId = req.query.pageId;

        Page.findByPk(pageId).then(page => {
            let frames = page.Frames;

            res.send(frames);
        }).catch(error => {
            res.json(responseHandler.createResponse(false, token, error));
        });
    }
};
