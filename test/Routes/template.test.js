/*
 * Project : ecomics-nodejs
 * FileName : template.test.js
 * Copyright (c) created by greg at 13.06.19
 */

const Path = require('path');
const HttpStatus = require('http-status-codes');
const errorMessage = require(Path.resolve('messages', 'error.json'));
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require(Path.resolve('app'));
const token = require('config').get('jwt.testToken');
const mocha = require('mocha');
const PathService = require(Path.resolve('services', 'Path.service'));
const baseUrl = PathService.getRoot();
const describe = mocha.describe;
const it = mocha.it;
const expect = chai.expect;

chai.should();
chai.use(chaiHttp);

const TemplateCrudUrl = Path.join('/', baseUrl, 'template');
const TemplateTagCrudUrl = Path.join('/', baseUrl, 'template', 'tag');

describe('Test template routes', () => {
    it('Get Template Nok : Without token', (done) => {
        chai.request(server).get(TemplateCrudUrl).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.jwt.notFound);
                done();
            }
        });
    });

    it('Get Template Nok : without params', (done) => {
        chai.request(server).get(TemplateCrudUrl + '?token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Get Template Nok : with float', (done) => {
        chai.request(server).get(TemplateCrudUrl + '?templateId=1.5&token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.format.id);

                done();
            }
        });
    });

    it('Add Template Nok : Without token', (done) => {
        chai.request(server).post(TemplateCrudUrl).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.jwt.notFound);
                done();
            }
        });
    });

    it('Add Template Nok : without params', (done) => {
        chai.request(server).post(TemplateCrudUrl).send({
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Add Template Nok : missing title', (done) => {
        chai.request(server).post(TemplateCrudUrl).send({
            description: 'myDescription',
            userId: '1',
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Add Template Nok : missing description', (done) => {
        chai.request(server).post(TemplateCrudUrl).send({
            title: 'myTitle',
            userId: '1',
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Add Template Nok : missing userId', (done) => {
        chai.request(server).post(TemplateCrudUrl).send({
            title: 'myTitle',
            description: 'myDescription',
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Add Template Nok : userId format and not found without miniature', (done) => {
        chai.request(server).post(TemplateCrudUrl).send({
            title: 'myTitle',
            description: 'myDescription',
            userId: 'a',
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);

                done();
            }
        });
    });

    it('Update Template Nok : missing token', (done) => {
        chai.request(server).put(TemplateCrudUrl).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.jwt.notFound);
                done();
            }
        });
    });

    it('Update Template Nok : missing params', (done) => {
        chai.request(server).put(TemplateCrudUrl).send({
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Update Template Nok : Id is not an integer', (done) => {
        chai.request(server).put(TemplateCrudUrl).send({
            templateId: '1.5',
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.format.id);
                done();
            }
        });
    });

    it('Delete Template Nok : missing token', (done) => {
        chai.request(server).delete(TemplateCrudUrl).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.jwt.notFound);
                done();
            }
        });
    });

    it('Delete Template Nok : missing params', (done) => {
        chai.request(server).delete(TemplateCrudUrl + '?token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Delete Template  Nok : Id is not an integer', (done) => {
        chai.request(server).delete(TemplateCrudUrl + '?templateId=a&token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.format.id);
                done();
            }
        });
    });

    it('Get Template Tag Nok : missing token', (done) => {
        chai.request(server).get(TemplateTagCrudUrl).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.jwt.notFound);
                done();
            }
        });
    });

    it('Get Template Tag Nok : With templateId null', (done) => {
        chai.request(server).get(TemplateTagCrudUrl + '?templateId=&token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Get Template Tag Nok : missing id param', (done) => {
        chai.request(server).get(TemplateTagCrudUrl + '?token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Get Template Tag Nok : Id is not an integer', (done) => {
        chai.request(server).get(TemplateTagCrudUrl + '?templateId=1.5&token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.format.id);
                done();
            }
        });
    });

    it('Add Template Tag Nok : missing token', (done) => {
        chai.request(server).post(TemplateTagCrudUrl).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.jwt.notFound);
                done();
            }
        });
    });

    it('Add Template Tag : Missing required params', (done) => {
        chai.request(server).post(TemplateTagCrudUrl).send({
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Add Template Tag Nok : Missing templateId params', (done) => {
        chai.request(server).post(TemplateTagCrudUrl).send({
            tagId: '123',
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Add Template Tag Nok : Missing tagId params', (done) => {
        chai.request(server).post(TemplateTagCrudUrl).send({
            templateId: '123',
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Add Template Tag Nok : tagId format', (done) => {
        chai.request(server).post(TemplateTagCrudUrl).send({
            templateId: '123',
            tagId: 'a',
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.format.id);
                done();
            }
        });
    });

    it('Add Template Tag Nok : templateId format', (done) => {
        chai.request(server).post(TemplateTagCrudUrl).send({
            templateId: 'a',
            tagId: '2',
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.format.id);
                done();
            }
        });
    });

    it('Remove Template Tag Nok : missing token', (done) => {
        chai.request(server).delete(TemplateTagCrudUrl).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.jwt.notFound);
                done();
            }
        });
    });

    it('Remove Template Tag : Missing required params', (done) => {
        chai.request(server).delete(TemplateTagCrudUrl + '?token=' + token).send({
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Remove Template Tag Nok : Missing templateId params', (done) => {
        chai.request(server).delete(TemplateTagCrudUrl + '?tagId=2&token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Remove Template Tag Nok : Missing tagId params', (done) => {
        chai.request(server).delete(TemplateTagCrudUrl + '?templateId=123&token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Remove Template Tag Nok : tagId format', (done) => {
        chai.request(server).delete(TemplateTagCrudUrl + '?templateId=123&tagId=a&token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.format.id);
                done();
            }
        });
    });

    it('Remove Template Tag Nok : templateId format', (done) => {
        chai.request(server).delete(TemplateTagCrudUrl + '?templateId=a&tagId=2&token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.format.id);
                done();
            }
        });
    });
});
