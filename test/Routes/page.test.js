/*
 * Project : ecomics-nodejs
 * FileName : page.test.js
 * Copyright (c) created by greg at 13.06.19
 */

const Path = require('path');
const HttpStatus = require('http-status-codes');
const errorMessage = require(Path.resolve('messages', 'error.json'));
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require(Path.resolve('app'));
const token = require('config').get('jwt.testToken');
const mocha = require('mocha');
const PathService = require(Path.resolve('services', 'Path.service'));
const baseUrl = PathService.getRoot();
const describe = mocha.describe;
const it = mocha.it;
const expect = chai.expect;

chai.should();
chai.use(chaiHttp);

const PageCrudUrl = Path.join('/', baseUrl, 'page');

describe('Test page routes', () => {
    it('Get Page Nok : Without token', (done) => {
        chai.request(server).get(PageCrudUrl).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.jwt.notFound);
                done();
            }
        });
    });

    it('Get Page Nok : missing params', (done) => {
        chai.request(server).get(PageCrudUrl + '?token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Get Page Nok : roleId format', (done) => {
        chai.request(server).get(PageCrudUrl + '?pageId=1a&token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.format.id);
                done();
            }
        });
    });

    it('Create Page Nok : Without token', (done) => {
        chai.request(server).post(PageCrudUrl).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.jwt.notFound);
                done();
            }
        });
    });

    it('Create Page Nok : missing params', (done) => {
        chai.request(server).post(PageCrudUrl).send({
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Create Page Nok : missing params templateId', (done) => {
        chai.request(server).post(PageCrudUrl).send({
            number: '1a',
            file: 'file',
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Create Page Nok : missing params number', (done) => {
        chai.request(server).post(PageCrudUrl).send({
            templateId: '1',
            file: 'file',
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Create Page Nok : templateId format', (done) => {
        chai.request(server).post(PageCrudUrl).send({
            number: '1',
            file: 'file',
            templateId: '1a',
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.format.id);
                done();
            }
        });
    });

    it('Create Page Nok : number format', (done) => {
        chai.request(server).post(PageCrudUrl).send({
            number: '1a',
            file: 'file',
            templateId: '1',
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.format.id);
                done();
            }
        });
    });

    it('Update Page Nok : withoutToken', (done) => {
        chai.request(server).put(PageCrudUrl).send({}).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.jwt.notFound);
                done();
            }
        });
    });

    it('Update Page Nok : without pageId', (done) => {
        chai.request(server).put(PageCrudUrl).send({
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Delete Page Nok : Without token', (done) => {
        chai.request(server).delete(PageCrudUrl).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.jwt.notFound);
                done();
            }
        });
    });

    it('Delete Page Nok : missing params', (done) => {
        chai.request(server).delete(PageCrudUrl + '?token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Delete Page Nok : roleId format', (done) => {
        chai.request(server).delete(PageCrudUrl + '?pageId=1a&token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.format.id);
                done();
            }
        });
    });
});
