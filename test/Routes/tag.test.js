/*
 * Project : ecomics-nodejs
 * FileName : tag.test.js
 * Copyright (c) created by greg at 13.06.19
 */

const Path = require('path');
const HttpStatus = require('http-status-codes');
const errorMessage = require(Path.resolve('messages', 'error.json'));
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require(Path.resolve('app'));
const token = require('config').get('jwt.testToken');
const mocha = require('mocha');
const PathService = require(Path.resolve('services', 'Path.service'));
const baseUrl = PathService.getRoot();
const describe = mocha.describe;
const it = mocha.it;
const expect = chai.expect;

chai.should();
chai.use(chaiHttp);

const TagCrudUrl = Path.join('/', baseUrl, 'tag');

describe('Test tag routes', () => {
    it('Get Tag Nok : Without token', (done) => {
        chai.request(server).get(TagCrudUrl).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.jwt.notFound);
                done();
            }
        });
    });

    it('Get Tag Nok : missing params', (done) => {
        chai.request(server).get(TagCrudUrl + '?token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Get Tag Nok : tagId format', (done) => {
        chai.request(server).get(TagCrudUrl + '?tagId=1a&token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.format.id);
                done();
            }
        });
    });

    it('Create Tag Nok : Without token', (done) => {
        chai.request(server).post(TagCrudUrl).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.jwt.notFound);
                done();
            }
        });
    });

    it('Create Tag Nok : missing params', (done) => {
        chai.request(server).post(TagCrudUrl).send({
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Create Tag Nok : tagId format', (done) => {
        chai.request(server).post(TagCrudUrl).send({
            tagId: '1a',
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Update Tag Nok : Without token', (done) => {
        chai.request(server).put(TagCrudUrl).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.jwt.notFound);
                done();
            }
        });
    });

    it('Update Tag Nok : missing params', (done) => {
        chai.request(server).put(TagCrudUrl).send({
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Update Tag Nok : Id is not an integer', (done) => {
        chai.request(server).put(TagCrudUrl).send({
            tagId: '1.5',
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.format.id);
                done();
            }
        });
    });

    it('Update Tag Nok : nothing to be update', (done) => {
        chai.request(server).put(TagCrudUrl).send({
            tagId: '1',
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.nothingUpdated);
                done();
            }
        });
    });

    it('Delete Tag Nok : Without token', (done) => {
        chai.request(server).delete(TagCrudUrl).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.jwt.notFound);
                done();
            }
        });
    });

    it('Delete Tag Nok : missing params', (done) => {
        chai.request(server).delete(TagCrudUrl + '?token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Delete Tag Nok : tagId format', (done) => {
        chai.request(server).delete(TagCrudUrl + '?tagId=1a&token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.format.id);
                done();
            }
        });
    });
});
