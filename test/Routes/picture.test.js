/*
 * Project : ecomics-nodejs
 * FileName : picture.test.js
 * Copyright (c) created by greg at 13.06.19
 */

const Path = require('path');
const chai = require('chai');
const mocha = require('mocha');
const chaiHttp = require('chai-http');
const server = require(Path.resolve('app'));
const HttpStatus = require('http-status-codes');
const errorMessage = require(Path.resolve('messages', 'error.json'));
const token = require('config').get('jwt.testToken');
const PathService = require(Path.resolve('services', 'Path.service'));
const baseUrl = PathService.getRoot();
const describe = mocha.describe;
const it = mocha.it;
const expect = chai.expect;

chai.should();
chai.use(chaiHttp);

const pictureCrudUrl = Path.join('/', baseUrl, 'picture');

describe('Test picture routes', () => {
    it('Get Picture Nok : Without token', (done) => {
        chai.request(server).get(pictureCrudUrl).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.jwt.notFound);
                done();
            }
        });
    });

    it('Get Picture Nok : With id null', (done) => {
        chai.request(server).get(pictureCrudUrl + '?pictureId=&token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Get Picture Nok : missing id param', (done) => {
        chai.request(server).get(pictureCrudUrl + '?token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Get Picture Nok : Id is not an integer', (done) => {
        chai.request(server).get(pictureCrudUrl + '?pictureId=1.5&token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.format.id);
                done();
            }
        });
    });

    it('Create Picture Nok : Without token', (done) => {
        chai.request(server).post(pictureCrudUrl).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.jwt.notFound);
                done();
            }
        });
    });

    it('Create Picture Nok : Missing params', (done) => {
        chai.request(server).post(pictureCrudUrl).send({
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Create Picture Nok : Missing  templateId params', (done) => {
        chai.request(server).post(pictureCrudUrl).send({
            file: '123',
            bdId: '1',
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Create Picture Nok : Missing  bdId params', (done) => {
        chai.request(server).post(pictureCrudUrl).send({
            file: '123',
            templateId: '1',
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Create Picture Nok : Missing  file params', (done) => {
        chai.request(server).post(pictureCrudUrl).send({
            templateId: '123',
            bdId: '1',
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Create Picture Nok : bdId format params', (done) => {
        chai.request(server).post(pictureCrudUrl).send({
            file: '1',
            bdId: '1a',
            frameId: '1',
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.format.id);
                done();
            }
        });
    });

    it('Create Picture Nok : frameId format params', (done) => {
        chai.request(server).post(pictureCrudUrl).send({
            file: '1',
            bdId: '1',
            frameId: '1a',
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.format.id);
                done();
            }
        });
    });

    it('Delete Picture Nok : Without token', (done) => {
        chai.request(server).delete(pictureCrudUrl).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.jwt.notFound);
                done();
            }
        });
    });

    it('Delete Picture Nok : missing params', (done) => {
        chai.request(server).delete(pictureCrudUrl + '?token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Delete Picture  Nok : Id is not an integer', (done) => {
        chai.request(server).delete(pictureCrudUrl + '?pictureId=a&token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.format.id);
                done();
            }
        });
    });
});
