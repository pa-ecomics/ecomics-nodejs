/*
 * Project : ecomics-nodejs
 * FileName : bd.test.js
 * Copyright (c) created by greg at 13.06.19
 */

const Path = require('path');
const chai = require('chai');
const mocha = require('mocha');
const chaiHttp = require('chai-http');
const server = require(Path.resolve('app'));
const HttpStatus = require('http-status-codes');
const errorMessage = require(Path.resolve('messages', 'error.json'));
const token = require('config').get('jwt.testToken');
const PathService = require(Path.resolve('services', 'Path.service'));
const baseUrl = PathService.getRoot();
const describe = mocha.describe;
const it = mocha.it;
const expect = chai.expect;

chai.should();
chai.use(chaiHttp);

const bdCrudUrl = Path.join('/', baseUrl, 'bd');

describe('Test bd routes', () => {
    it('Get Bd Nok : Without token', (done) => {
        chai.request(server).get(bdCrudUrl).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.jwt.notFound);
                done();
            }
        });
    });

    it('Get Bd Nok : With id null', (done) => {
        chai.request(server).get(bdCrudUrl + '?bdId=&token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Get Bd Nok : missing id param', (done) => {
        chai.request(server).get(bdCrudUrl + '?token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Get Bd Nok : Id is not an integer', (done) => {
        chai.request(server).get(bdCrudUrl + '?bdId=1.5&token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.format.id);
                done();
            }
        });
    });

    it('Create Bd Nok : Without token', (done) => {
        chai.request(server).post(bdCrudUrl).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.jwt.notFound);
                done();
            }
        });
    });

    it('Create Bd Nok : Missing params', (done) => {
        chai.request(server).post(bdCrudUrl).send({
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Create Bd Nok : Missing  userId params', (done) => {
        chai.request(server).post(bdCrudUrl).send({
            userId: '123',
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Create Bd Nok : Missing  templateId params', (done) => {
        chai.request(server).post(bdCrudUrl).send({
            templateId: '123',
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Create Bd Nok : templateId format params', (done) => {
        chai.request(server).post(bdCrudUrl).send({
            userId: '1',
            templateId: '1.5',
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.format.id);
                done();
            }
        });
    });

    it('Create Bd Nok : userId format params', (done) => {
        chai.request(server).post(bdCrudUrl).send({
            userId: '1a',
            templateId: '1',
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.format.id);
                done();
            }
        });
    });

    it('Delete Bd Nok : Without token', (done) => {
        chai.request(server).delete(bdCrudUrl).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.jwt.notFound);
                done();
            }
        });
    });

    it('Delete Bd Nok : missing params', (done) => {
        chai.request(server).delete(bdCrudUrl + '?token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Delete Bd  Nok : Id is not an integer', (done) => {
        chai.request(server).delete(bdCrudUrl + '?bdId=a&token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.format.id);
                done();
            }
        });
    });
});
