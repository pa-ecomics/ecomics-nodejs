/*
 * Project : ecomics-nodejs
 * FileName : user.test.js
 * Copyright (c) created by greg at 13.06.19
 */

const Path = require('path');
const chai = require('chai');
const mocha = require('mocha');
const chaiHttp = require('chai-http');
const server = require(Path.resolve('app'));
const HttpStatus = require('http-status-codes');
const errorMessage = require(Path.resolve('messages', 'error.json'));
const token = require('config').get('jwt.testToken');
const PathService = require(Path.resolve('services', 'Path.service'));
const baseUrl = PathService.getRoot();
const describe = mocha.describe;
const it = mocha.it;
const expect = chai.expect;

chai.should();
chai.use(chaiHttp);

const userCrudUrl = Path.join('/', baseUrl, 'user');
const userAthenticateUrl = Path.join(userCrudUrl, 'authenticate');
const userRoleCrud = Path.join(userCrudUrl, 'role');

describe('Test user routes', () => {
    it('Get User Nok : With id null', (done) => {
        chai.request(server).get(userCrudUrl + '?userId=&token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Get User Nok : missing id param', (done) => {
        chai.request(server).get(userCrudUrl + '?token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Get User Nok : Without token', (done) => {
        chai.request(server).get(userCrudUrl).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.jwt.notFound);
                done();
            }
        });
    });

    it('Get User Nok : Id is not an integer', (done) => {
        chai.request(server).get(userCrudUrl + '?userId=1.5&token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.format.id);
                done();
            }
        });
    });

    it('Create User Nok : Missing required params', (done) => {
        chai.request(server).post(userCrudUrl).send({
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Create User Nok : Missing email params', (done) => {
        chai.request(server).post(userCrudUrl).send({
            password: '123',
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Create User Nok : Missing password params', (done) => {
        chai.request(server).post(userCrudUrl).send({
            pseudo: '123',
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Update User Nok : Without token', (done) => {
        chai.request(server).put(userCrudUrl).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.jwt.notFound);
                done();
            }
        });
    });

    it('Update User Nok : missing params', (done) => {
        chai.request(server).put(userCrudUrl).send({
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Update User Nok : Id is not an integer', (done) => {
        chai.request(server).put(userCrudUrl).send({
            userId: '1.5',
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);
                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.format.id);

                done();
            }
        });
    });

    it('Delete User Nok : Without token', (done) => {
        chai.request(server).delete(userCrudUrl).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.jwt.notFound);
                done();
            }
        });
    });

    it('Delete User Nok : missing params', (done) => {
        chai.request(server).delete(userCrudUrl + '?token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Delete User  Nok : Id is not an integer', (done) => {
        chai.request(server).delete(userCrudUrl + '?userId=a&token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);
                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.format.id);

                done();
            }
        });
    });

    it('Authenticate Password User Nok : Missing params', (done) => {
        chai.request(server).post(userAthenticateUrl).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Authenticate Password User Nok : Missing email', (done) => {
        chai.request(server).post(userAthenticateUrl).send({
            password: 'azdazdazd'
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Authenticate Password User Nok : Missing password', (done) => {
        chai.request(server).post(userAthenticateUrl).send({
            email: 'azdazdazd'
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Authenticate SocialNetwork User Nok : Missing params', (done) => {
        chai.request(server).put(userAthenticateUrl).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Authenticate SocialNetwork User Nok : Missing params', (done) => {
        chai.request(server).put(userAthenticateUrl).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Authenticate SocialNetwork User Nok : Missing email', (done) => {
        chai.request(server).put(userAthenticateUrl).send({
            socialName: 'azdazdazd',
            socialId: 'azdazdazd'
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Authenticate SocialNetwork User Nok : Missing socialName', (done) => {
        chai.request(server).put(userAthenticateUrl).send({
            email: 'azdazdazd',
            socialId: 'azdazdazd'
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Authenticate SocialNetwork User Nok : Missing socialId', (done) => {
        chai.request(server).put(userAthenticateUrl).send({
            socialName: 'azdazdazd',
            email: 'azdazdazd'
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Get User Role Nok : Without token', (done) => {
        chai.request(server).get(userRoleCrud).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.jwt.notFound);
                done();
            }
        });
    });

    it('Get User Role Nok : With userId null', (done) => {
        chai.request(server).get(userRoleCrud + '?userId=&token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Get User Role Nok : missing id param', (done) => {
        chai.request(server).get(userRoleCrud + '?token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Get User Role Nok : Id is not an integer', (done) => {
        chai.request(server).get(userRoleCrud + '?userId=1.5&token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);
                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.format.id);

                done();
            }
        });
    });

    it('Add User Role Nok : Without token', (done) => {
        chai.request(server).post(userRoleCrud).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.jwt.notFound);
                done();
            }
        });
    });

    it('Add User Role : Missing required params', (done) => {
        chai.request(server).post(userRoleCrud).send({
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Add User Role Nok : Missing userId params', (done) => {
        chai.request(server).post(userRoleCrud).send({
            roleId: '123',
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Add User Role Nok : Missing roleId params', (done) => {
        chai.request(server).post(userRoleCrud).send({
            userId: '123',
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Add User Role Nok : roleId format', (done) => {
        chai.request(server).post(userRoleCrud).send({
            userId: '123',
            roleId: 'a',
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);
                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.format.id);

                done();
            }
        });
    });

    it('Add User Role Nok : userId format', (done) => {
        chai.request(server).post(userRoleCrud).send({
            userId: 'a',
            roleId: '2',
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);
                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.format.id);

                done();
            }
        });
    });

    it('Remove User Role Nok : Without token', (done) => {
        chai.request(server).delete(userRoleCrud).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.jwt.notFound);
                done();
            }
        });
    });

    it('Remove User Role : Missing required params', (done) => {
        chai.request(server).delete(userRoleCrud + '?token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Remove User Role Nok : Missing userId params', (done) => {
        chai.request(server).delete(userRoleCrud + '?roleId=123&token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Remove User Role Nok : Missing roleId params', (done) => {
        chai.request(server).delete(userRoleCrud + '?userId=123&token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Remove User Role Nok : roleId format', (done) => {
        chai.request(server).delete(userRoleCrud + '?userId=123&roleId=a&token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);
                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.format.id);

                done();
            }
        });
    });

    it('Remove User Role Nok : userId format', (done) => {
        chai.request(server).delete(userRoleCrud + '?userId=a&roleId=2&token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);
                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.format.id);

                done();
            }
        });
    });
});
