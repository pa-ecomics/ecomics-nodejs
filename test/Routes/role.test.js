/*
 * Project : ecomics-nodejs
 * FileName : role.test.js
 * Copyright (c) created by greg at 13.06.19
 */

const Path = require('path');
const HttpStatus = require('http-status-codes');
const errorMessage = require(Path.resolve('messages', 'error.json'));
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require(Path.resolve('app'));
const mocha = require('mocha');
const token = require('config').get('jwt.testToken');
const PathService = require(Path.resolve('services', 'Path.service'));
const baseUrl = PathService.getRoot();
const describe = mocha.describe;
const it = mocha.it;
const expect = chai.expect;

chai.should();
chai.use(chaiHttp);

const RoleCrudUrl = Path.join('/', baseUrl, 'role');

describe('Test role routes', () => {
    it('Get Role Nok : Without token', (done) => {
        chai.request(server).get(RoleCrudUrl).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.jwt.notFound);
                done();
            }
        });
    });

    it('Get Role Nok : missing params', (done) => {
        chai.request(server).get(RoleCrudUrl + '?token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Get Role Nok : roleId format', (done) => {
        chai.request(server).get(RoleCrudUrl + '?roleId=1a&token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.format.id);
                done();
            }
        });
    });

    it('Create Role Nok : Without token', (done) => {
        chai.request(server).post(RoleCrudUrl).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.jwt.notFound);
                done();
            }
        });
    });

    it('Create Role Nok : missing params', (done) => {
        chai.request(server).post(RoleCrudUrl).send({
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Update Role Nok : Without token', (done) => {
        chai.request(server).put(RoleCrudUrl).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.jwt.notFound);
                done();
            }
        });
    });

    it('Update Role Nok : missing params', (done) => {
        chai.request(server).put(RoleCrudUrl).send({
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Update Role Nok : Id is not an integer', (done) => {
        chai.request(server).put(RoleCrudUrl).send({
            roleId: '1.5',
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.format.id);
                done();
            }
        });
    });

    it('Update Role Nok : nothing to be update', (done) => {
        chai.request(server).put(RoleCrudUrl).send({
            roleId: '1',
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.nothingUpdated);
                done();
            }
        });
    });

    it('Delete Role Nok : Without token', (done) => {
        chai.request(server).delete(RoleCrudUrl).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.jwt.notFound);
                done();
            }
        });
    });

    it('Delete Role Nok : missing params', (done) => {
        chai.request(server).delete(RoleCrudUrl + '?token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Delete Role Nok : roleId format', (done) => {
        chai.request(server).delete(RoleCrudUrl + '?roleId=1a&token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.format.id);
                done();
            }
        });
    });
});
