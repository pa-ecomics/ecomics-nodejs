/*
 * Project : ecomics-nodejs
 * FileName : frame.test.js
 * Copyright (c) created by greg at 13.06.19
 */

const Path = require('path');
const chai = require('chai');
const mocha = require('mocha');
const chaiHttp = require('chai-http');
const server = require(Path.resolve('app'));
const HttpStatus = require('http-status-codes');
const errorMessage = require(Path.resolve('messages', 'error.json'));
const token = require('config').get('jwt.testToken');
const PathService = require(Path.resolve('services', 'Path.service'));
const baseUrl = PathService.getRoot();
const describe = mocha.describe;
const it = mocha.it;
const expect = chai.expect;

chai.should();
chai.use(chaiHttp);

const frameCrudUrl = Path.join('/', baseUrl, 'frame');

describe('Test frame routes', () => {
    it('Get Frame Nok : Without token', (done) => {
        chai.request(server).get(frameCrudUrl).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.jwt.notFound);
                done();
            }
        });
    });

    it('Get Frame Nok : With id null', (done) => {
        chai.request(server).get(frameCrudUrl + '?frameId=&token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Get Frame Nok : missing id param', (done) => {
        chai.request(server).get(frameCrudUrl + '?token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Get Frame Nok : Id is not an integer', (done) => {
        chai.request(server).get(frameCrudUrl + '?frameId=1.5&token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.format.id);
                done();
            }
        });
    });

    it('Create Frame Nok : Without token', (done) => {
        chai.request(server).post(frameCrudUrl).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.jwt.notFound);
                done();
            }
        });
    });

    it('Create Frame Nok : Missing params', (done) => {
        chai.request(server).post(frameCrudUrl).send({
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Create Frame Nok : Missing  pageId params', (done) => {
        chai.request(server).post(frameCrudUrl).send({
            file: '123',
            positionX: '123',
            positionY: '123',
            width: '123',
            height: '123',
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Create Frame Nok : Missing  height params', (done) => {
        chai.request(server).post(frameCrudUrl).send({
            file: '123',
            positionX: '123',
            positionY: '123',
            width: '123',
            pageId: '123',
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Create Frame Nok : Missing  width params', (done) => {
        chai.request(server).post(frameCrudUrl).send({
            file: '123',
            positionX: '123',
            positionY: '123',
            height: '123',
            pageId: '123',
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Create Frame Nok : Missing  positionY params', (done) => {
        chai.request(server).post(frameCrudUrl).send({
            file: '123',
            positionX: '123',
            width: '123',
            height: '123',
            pageId: '123',
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Create Frame Nok : Missing  positionX params', (done) => {
        chai.request(server).post(frameCrudUrl).send({
            file: '123',
            positionY: '123',
            width: '123',
            height: '123',
            pageId: '123',
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Create Frame Nok : pageId format', (done) => {
        chai.request(server).post(frameCrudUrl).send({
            file: 'fileName',
            positionX: '123',
            positionY: '123',
            width: '123',
            height: '123',
            pageId: '1a',
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.format.id);
                done();
            }
        });
    });

    it('Update Frame Nok : Without token', (done) => {
        chai.request(server).put(frameCrudUrl).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.jwt.notFound);
                done();
            }
        });
    });

    it('Update Frame Nok : missing params', (done) => {
        chai.request(server).put(frameCrudUrl).send({
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Update Frame Nok : Id is not an integer', (done) => {
        chai.request(server).put(frameCrudUrl).send({
            frameId: '1.5',
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.format.id);
                done();
            }
        });
    });

    it('Update Frame Nok : nothing to be update', (done) => {
        chai.request(server).put(frameCrudUrl).send({
            frameId: '1',
            token: token
        }).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.nothingUpdated);
                done();
            }
        });
    });

    it('Delete Frame Nok : Without token', (done) => {
        chai.request(server).delete(frameCrudUrl).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.jwt.notFound);
                done();
            }
        });
    });

    it('Delete Frame Nok : missing params', (done) => {
        chai.request(server).delete(frameCrudUrl + '?token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                res.should.have.status(HttpStatus.NOT_FOUND);
                done();
            }
        });
    });

    it('Delete Frame  Nok : Id is not an integer', (done) => {
        chai.request(server).delete(frameCrudUrl + '?frameId=a&token=' + token).end((err, res) => {
            if (err) {
                done(err);
            } else {
                let response = JSON.parse(res.text);

                expect(response.succeed).to.be.false;
                expect(response.message).eq(errorMessage.format.id);
                done();
            }
        });
    });
});
