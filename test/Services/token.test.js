/*
 * Project : ecomics-nodejs
 * FileName : token.test.js
 * Copyright (c) created by greg at 13.06.19
 */

const Path = require('path');
const chai = require('chai');
const mocha = require('mocha');
const tokenService = require(Path.resolve('services', 'Token.service'));
const token = require('config').get('jwt.testToken');
const expect = chai.expect;
const describe = mocha.describe;
const it = mocha.it;

describe('Token service', () => {
    it('Token Sign Ok : Empty data', (done) => {
        let token = tokenService.sign({});

        expect(token).to.be.a('string');
        expect(token).to.be.not.null;
        done();
    });

    it('Token Ok : Some data', (done) => {
        let userId = 1;

        let token = tokenService.sign({ userId: userId });
        expect(token).to.be.a('string');
        expect(token).to.be.not.null;

        tokenService.verify(token).then((decoded) => {
            expect(decoded).to.be.a('object');
            expect(decoded).to.be.not.null;
            expect(decoded.userId).to.be.eq(userId);

            done();
        }).catch(error => {
            done(error);
        });
    });

    it('Token Ok : Complex data', (done) => {
        let user = { id: 1, name: 'Greg' };

        let token = tokenService.sign({ user: user });
        expect(token).to.be.a('string');
        expect(token).to.be.not.null;

        tokenService.verify(token).then((decoded) => {
            expect(decoded).to.be.a('object');
            expect(decoded).to.be.not.null;
            expect(decoded.user.id).to.be.eq(user.id);
            expect(decoded.user.name).to.be.eq(user.name);

            done();
        }).catch(error => {
            done(error);
        });
    });

    it('Nok : Change token', (done) => {
        let token = tokenService.sign({});
        expect(token).to.be.a('string');
        expect(token).to.be.not.null;

        tokenService.verify(token + 'G').then((decoded) => {
            done('Doken shoold be not good !!!');
        }).catch(error => {
            expect(error).to.be.a('string');
            done();
        });
    });

    it('Nok : Change token', (done) => {
        let token = tokenService.sign({});
        expect(token).to.be.a('string');
        expect(token).to.be.not.null;

        tokenService.verify(token + 'G').then((decoded) => {
            done('Doken shoold be not good !!!');
        }).catch(error => {
            expect(error).to.be.a('string');
            done();
        });
    });

    it('Ok : Decode token', (done) => {
        let decoded = tokenService.decode(token);

        expect(decoded).to.be.a('object');
        expect(decoded.userId).to.be.eq(6);
        done();
    });
});
