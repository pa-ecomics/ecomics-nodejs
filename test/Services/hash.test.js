/*
 * Project : ecomics-nodejs
 * FileName : hash.test.js
 * Copyright (c) created by greg at 13.06.19
 */

const Path = require('path');
const chai = require('chai');
const mocha = require('mocha');
const HashService = require(Path.resolve('services', 'Hash.service'));
const expect = chai.expect;
const describe = mocha.describe;
const it = mocha.it;

const simpleText = 'pokfonzef';
const simpleTextHash = '$2b$10$cmiRH5LXoV5luu3ZD21JAOd2s.WZgZnvXlxC9NEegGSLEWxiVCGuO';
const otherText = 'fzepoajzf';

describe('Hash service', () => {
    it('simple hash', (done) => {
        let hash = HashService.crypt(simpleText);

        expect(hash).to.be.a('string');
        done();
    });

    it('simple verify true', (done) => {
        let check = HashService.compare(simpleText, simpleTextHash);
        expect(check).to.be.true;
        done();
    });

    it('simple verify false', (done) => {
        let check = HashService.compare(simpleText, otherText);
        expect(check).to.be.false;
        done();
    });

    it('simple hash and verify ok', (done) => {
        let hash = HashService.crypt(simpleText);
        let compare = HashService.compare(simpleText, hash);

        expect(hash).to.be.a('string');
        expect(compare).to.be.true;
        done();
    });

    it('simple hash and verify nok', (done) => {
        let hash = HashService.crypt(simpleText);
        let compare = HashService.compare(otherText, hash);

        expect(hash).to.be.a('string');
        expect(compare).to.be.false;
        done();
    });
});
