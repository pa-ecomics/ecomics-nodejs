/*
 * Project : ecomics-nodejs
 * FileName : mailer.test.js
 * Copyright (c) created by greg at 13.06.19
 */

const Path = require('path');
const chai = require('chai');
const mocha = require('mocha');
const MailerService = require(Path.resolve('services', 'Mailer.service'));
const errorMessage = require(Path.resolve('messages', 'error.json')).services.mailer;
const configMailer = require('config').get('mailer');
const expect = chai.expect;
const describe = mocha.describe;
const it = mocha.it;

const email = configMailer.login;
const object = 'Ecomics Test';

chai.should();

describe('Mail service', () => {
    it('Send an email with simple text', function (done) {
        this.timeout(3000);
        MailerService.send({ to: email, subject: object, text: 'My simple text !' }).then(response => {
            expect(response).should.be.ok;
            done();
        }).catch(error => {
            done(error);
        });
    });

    it('Send an email with html', function (done) {
        this.timeout(3000);
        MailerService.send({ to: email, subject: object, html: '<h1>My simple html !</h1>' }).then(response => {
            expect(response).should.be.ok;
            done();
        }).catch(error => {
            done(error);
        });
    });

    it('Send an email with simple text and html', function (done) {
        this.timeout(3000);
        MailerService.send({ to: email, subject: object, text: 'My Text !' }).then(response => {
            response.should.be.ok;
            done();
        }).catch(error => {
            done(error);
        });
    });

    it('Send without subject', function (done) {
        MailerService.send({ to: email, text: 'My Text without subject !' }).then(response => {
            done('Mail should not be send without subject');
        }).catch(error => {
            expect(error).to.be.a('Error');
            expect(error.message).to.be.eq(errorMessage.missing.subject);
            done();
        });
    });

    it('Missing destination address', function (done) {
        MailerService.send({ subject: object, text: 'My Text without address !' }).then(response => {
            done('Mail should not be send without destination address');
        }).catch(error => {
            expect(error).to.be.a('Error');
            expect(error.message).to.be.eq(errorMessage.missing.destination);
            done();
        });
    });

    it('Missing body', function (done) {
        MailerService.send({ to: email, subject: object }).then(response => {
            done('Mail should not be send without body');
        }).catch(error => {
            expect(error).to.be.a('Error');
            expect(error.message).to.be.eq(errorMessage.missing.body);
            done();
        });
    });

    it('Send without object and body', function (done) {
        MailerService.send({ to: email }).then(response => {
            done('Mail should not be send without object and body');
        }).catch(error => {
            expect(error).to.be.a('Error');
            expect(error.message).to.be.eq(errorMessage.missing.subject);
            done();
        });
    });
});
