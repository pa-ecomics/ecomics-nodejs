/*
 * Project : ecomics-nodejs
 * FileName : url.test.js
 * Copyright (c) created by greg at 13.06.19
 */

const Path = require('path');
const PathService = require(Path.resolve('services', 'Path.service'));
const chai = require('chai');
const mocha = require('mocha');
const expect = chai.expect;
const describe = mocha.describe;
const it = mocha.it;

describe('Url service', () => {
    it('Get Root with root', (done) => {
        let path = PathService.getRoot(true);

        expect(path).to.be.eq(Path.join('/'));
        done();
    });

    it('Get public back', (done) => {
        let path = PathService.getPublic(true, false);

        expect(path).to.be.eq(Path.join('public', 'api'));
        done();
    });

    it('Get public back with root', (done) => {
        let path = PathService.getPublic(true, true);

        expect(path).to.be.eq(Path.join('/', 'public', 'api'));
        done();
    });

    it('Get public front', (done) => {
        let path = PathService.getPublic(false, false);

        expect(path).to.be.eq('api');
        done();
    });

    it('Get public front with root', (done) => {
        let path = PathService.getPublic(false, true);

        expect(path).to.be.eq(Path.join('/', 'api'));
        done();
    });

    it('Get public without params', (done) => {
        let path = PathService.getPublic();

        expect(path).to.be.eq('api');
        done();
    });

    it('Get template back with root', (done) => {
        let path = PathService.getTemplate(true, true);

        expect(path).to.be.eq(Path.join('/', 'public', 'api', 'shared', 'template'));
        done();
    });

    it('Get template back without root', (done) => {
        let path = PathService.getTemplate(true, false);

        expect(path).to.be.eq(Path.join('public', 'api', 'shared', 'template'));
        done();
    });

    it('Get template front with root', (done) => {
        let path = PathService.getTemplate(false, true);

        expect(path).to.be.eq(Path.join('/', 'api', 'shared', 'template'));
        done();
    });

    it('Get template front without root', (done) => {
        let path = PathService.getTemplate(false, false);

        expect(path).to.be.eq(Path.join('api', 'shared', 'template'));
        done();
    });
});
