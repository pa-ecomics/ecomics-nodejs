/*
 * Project : ecomics-nodejs
 * FileName : number.test.js
 * Copyright (c) created by greg at 13.06.19
 */

const Path = require('path');
const chai = require('chai');
const mocha = require('mocha');
const NumberService = require(Path.resolve('services', 'Number.service'));
const expect = chai.expect;
const describe = mocha.describe;
const it = mocha.it;

describe('Number service', () => {
    it('isInteger Ok : simple digit', (done) => {
        expect(NumberService.isInteger(0)).to.be.true;
        done();
    });

    it('isInteger Ok : simple digit String', (done) => {
        expect(NumberService.isInteger('0')).to.be.true;
        done();
    });

    it('isInteger Ok : simple other digit', (done) => {
        expect(NumberService.isInteger(8)).to.be.true;
        done();
    });

    it('isInteger Ok : simple other digit String', (done) => {
        expect(NumberService.isInteger('8')).to.be.true;
        done();
    });

    it('isInteger Ok : simple digit negative', (done) => {
        expect(NumberService.isInteger(-7)).to.be.true;
        done();
    });

    it('isInteger Ok : simple digit negative String', (done) => {
        expect(NumberService.isInteger('-7')).to.be.true;
        done();
    });

    it('isInteger Ok : simple number', (done) => {
        expect(NumberService.isInteger(87)).to.be.true;
        done();
    });

    it('isInteger Ok : simple number String', (done) => {
        expect(NumberService.isInteger('87')).to.be.true;
        done();
    });

    it('isInteger Ok : simple number negative', (done) => {
        expect(NumberService.isInteger(-57)).to.be.true;
        done();
    });

    it('isInteger Ok : simple number negative String', (done) => {
        expect(NumberService.isInteger('57')).to.be.true;
        done();
    });

    it('isInteger Nok : simple letter', (done) => {
        expect(NumberService.isInteger('a')).to.be.false;
        done();
    });

    it('isInteger Nok : simple word', (done) => {
        expect(NumberService.isInteger('azerty')).to.be.false;
        done();
    });

    it('isInteger Nok : simple float (dot)', (done) => {
        expect(NumberService.isInteger(1.5)).to.be.false;
        done();
    });

    it('isInteger Nok : simple float string (dot)', (done) => {
        expect(NumberService.isInteger('1.5')).to.be.false;
        done();
    });

    it('isInteger Nok : simple float string (comma)', (done) => {
        expect(NumberService.isInteger('1,5')).to.be.false;
        done();
    });

    it('isInteger Nok : simple float negative (dot)', (done) => {
        expect(NumberService.isInteger(-1.5)).to.be.false;
        done();
    });

    it('isInteger Nok : simple float negative string (dot)', (done) => {
        expect(NumberService.isInteger('-1.5')).to.be.false;
        done();
    });

    it('isInteger Nok : simple float negative string (comma)', (done) => {
        expect(NumberService.isInteger('-1,5')).to.be.false;
        done();
    });

    it('isInteger Nok : digit start with character', (done) => {
        expect(NumberService.isInteger('a1')).to.be.false;
        done();
    });

    it('isInteger Nok : digit end with character', (done) => {
        expect(NumberService.isInteger('1a')).to.be.false;
        done();
    });

    it('isInteger Nok : digit contains character', (done) => {
        expect(NumberService.isInteger('1a1')).to.be.false;
        done();
    });
});
