/*
 * Project : ecomics-nodejs
 * FileName : pdf.test.js
 * Copyright (c) created by greg at 13.06.19
 */

const Path = require('path');
const fs = require('file-system');
const mocha = require('mocha');
const PdfService = require(Path.resolve('services', 'Pdf.service'));
const describe = mocha.describe;
const it = mocha.it;

const PathService = require(Path.resolve('services', 'Path.service'));

const resourcePath = PathService.getPublic(true);

describe('Pdf service', () => {
    it('PDF Service', (done) => {
        let pdfPath = 'testMocha.pdf';
        PdfService.createPdf(pdfPath, 'My own Text Greg');

        let basePath = Path.join(resourcePath, 'pdf', pdfPath);
        if (fs.existsSync(basePath)) {
            fs.unlinkSync(basePath);
            done();
        } else {
            done('Pdf not found !');
        }
    });
});
